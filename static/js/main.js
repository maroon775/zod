!function ($)
{

	"use strict";

	var bEvent = function ()
	{

		// A jQuery plugin for fluid width video embeds
		function responsiveVideo()
		{
			if ($.fn.fitVids)
			{
				$(".video").fitVids()
			}
		}

		// A jQuery plugin to create and manage Google Maps to jQuery
		function googleMap()
		{
			if(!('GMaps' in window)) return;

			var map = new GMaps({
				div                   : '#map',
				lat                   : 43.054092,
				lng                   : 74.463904,
				maxZoom               : 16,
				mapTypeControl        : true,
				scrollwheel           : false,
				disableDoubleClickZoom: true
			});
			var infowindow = new google.maps.InfoWindow({
				content: '<div style="width:320px;"><img src="/assets/img/bg/logosmall.jpg" alt="" style="float:left;"/><div style="float:left;"><ul class="list-unstyled maplist"><li><i class="fa fa-map-marker info-color"></i> АВК  «Манас 2» </li><li><i class="fa fa-envelope info-color"></i> example@airport.kg</li><li><i class="fa fa-phone info-color"></i> + (996) 312  69-31-20</li></ul></div></div>'
			});
			var marker = map.addMarker({
				lat  : 43.054092,
				lng  : 74.463904,
				title: 'ЗОД - Манас'
			});
			infowindow.open(map, marker);
		}

		// A custom select for Bootstrap using button dropdown
		function selectpicker()
		{
			if ($.fn.selectpicker)
			{
				$(".selectpicker").selectpicker()
			}
		}

		// A jQuery plugin for the navigation on one-page sites
		function navigation()
		{
			if ($.fn.onePageNav && $("#nav").length)
			{
				$('#nav').onePageNav({
					currentClass   : 'active',
					scrollSpeed    : 600,
					scrollOffset   : 60,
					scrollThreshold: 0.2,
					easing         : 'swing',
					filter         : ':not(.external)'
				})
			}
		}

		// Preloader
		function preloader()
		{
			$(window).load(function ()
			{
				$(".preloader").fadeOut()
				$("body").removeClass("remove-scroll")
			});
		}

		// A jQuery plugin that enables HTML5 placeholder behavior for browsers that aren’t trying hard enough yet
		function placeholderIE()
		{
			if ($.fn.placeholder)
			{
				$("input, textarea").placeholder()
			}
		}

		// validation and sending forms
		function validateAndSend()
		{
			$.validate({
				form          : '#registrationForm',
				validateOnBlur: false,
				addSuggestions: false,
				onSuccess     : function ()
				{
					var name = $("#regName").val(),
					    email = $("#regMail").val(),
					    phone = $("#regPhone").val(),
					    plan = $("#regPlan").val(),
					    allData = 'name=' + name + '&email=' + email + '&phone=' + phone + '&plan=' + plan;
					$.ajax({
						type   : "POST",
						url    : "php/register.php",
						data   : allData,
						success: function ()
						{
							$(".register").addClass("success")
							$("#regName").val("")
							$("#regMail").val("")
							$("#regPhone").val("")
						}
					});
					return false;
				}
			})

		}

		// Reveal Animations When You Scroll
		function wow()
		{
			$(window).load(function ()
			{
				new WOW().init()
			});
		}

		// Custom scripts
		function app()
		{
			$("#back").on("click", function ()
			{
				$(".register").removeClass("success")
			})
		}

		// Return all functions
		return {
			init: function ()
			{
				preloader()
				responsiveVideo()
				googleMap()
				selectpicker()
				navigation()
				placeholderIE()
				validateAndSend()
				wow()
				app()
			}
		}
	}();

	$(function ()
	{
		// Launch functions
		bEvent.init()
	})
}(window.jQuery);
