!function ($)
{

	"use strict";

	window.bAuth = function ()
	{

		var bootstrap_alert = function ()
		{
		};

		function settings()
		{
			$.ajaxSetup({
				type    : "POST",
				dataType: "json",
				error   : function (XMLHTTPObj)
				{
					alert("Произошла ошибка при выполнении AJAX запроса, возможно страница не найдена. Ответ от сервера" + XMLHTTPObj.responseText);
				}
			});

			bootstrap_alert.warning = function (message)
			{
				$('#alert_placeholder').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> ' + message + '</div>')
			}
			bootstrap_alert.success = function (message)
			{
				$('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Cool!</strong> ' + message + '</div>')
			}
		}

		function navigation()
		{
			if ($.fn.onePageNav && $("#nav").length)
			{
				$('#nav').onePageNav({
					currentClass   : 'active',
					scrollSpeed    : 600,
					scrollOffset   : 60,
					scrollThreshold: 0.2,
					easing         : 'swing',
					filter         : ':not(.external)'
				})
			}
		}

		// Preloader
		function preloader()
		{
			$(window).load(function ()
			{
				$(".preloader").fadeOut()
				$("body").removeClass("remove-scroll")
			});
		}

		// A jQuery plugin that enables HTML5 placeholder behavior for browsers that aren’t trying hard enough yet
		function placeholderIE()
		{
			if ($.fn.placeholder)
			{
				$("input, textarea").placeholder()
			}
		}

		// Custom scripts
		function app()
		{
			$("#loginForm").unbind('submit.afterValidateForm').one("submit.afterValidateForm", function (e)
			{
				console.log('submit.afterValidateForm');
				e.preventDefault();
				var allData = $("#loginForm").serialize();
				$.ajax({
					url    : "/auth/",
					data   : allData,
					success: function (msg)
					{
						if (msg.state == 'ok')
						{
							document.location = msg.answer;
						} else
						{
							bootstrap_alert.warning(msg.answer);
							//$('.btn-u').prop("disabled", false).children('i').removeClass('fa-spin fa-spinner').addClass('fa-user');
						}
					}
				});
				return false;
			});
			console.log('bind - submit.afterValidateForm');
			return true;
		}

		// Return all functions
		return {
			init : function ()
			{
				preloader();
				settings();
				navigation();
				placeholderIE();

			},
			login: function ()
			{
				console.log('execute - bAuth.login');
				return app();
			}
		}
	}();

	$(function ()
	{
		// Launch functions
		bAuth.init()
	})
}(window.jQuery);
