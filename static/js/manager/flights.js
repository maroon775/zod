!function ($) {

  "use strict";

  var bEvent = function () {
	
	var bootstrap_alert = function() {}
	var weekObj={'mon' : "пн",
				'tue' : "вт",
				'wed' : "ср",
				'thu' : "чт",
				'fri' : "пт",
				'sat' : "сб",
				'sun' : "вс"};
	
	function addslashes( str ) {    // Quote string with slashes
		 return (str + '')
		.replace(/[\[\]]/g, '\\$&');
	}

	function settings() {
		$.ajaxSetup({
			type: "POST",
			dataType:"json",
			error: function(XMLHTTPObj) {
				alert("Произошла ошибка при выполнении AJAX запроса, возможно страница не найдена. Ответ от сервера"+XMLHTTPObj.responseText);
			}
		});
		bootstrap_alert.warning = function(message) {
			$('.alert_placeholder').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> '+message+'</div>')
		}
		bootstrap_alert.success = function(message) {
			$('.alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Cool!</strong> '+message+'</div>')
		}
	}
    // Preloader
    function preloader() {
      $(window).load(function () {
        $(".preloader").fadeOut()
        $("body").removeClass("remove-scroll")
      });
    }
    // A jQuery plugin that enables HTML5 placeholder behavior for browsers that aren’t trying hard enough yet
    function placeholderIE() {
      if ($.fn.placeholder) {
        $("input, textarea").placeholder()
      }
    }
	
	function emptyForm() {
		$('input[type=text]').val('');
		$('select').val(0);
		$('input[type=checkbox]').prop('checked', '');
		$('.timepicker').hide().timepicker('setTime', '00:00');
		$('.alert_placeholder').html('');
	}
	
	function generateFlightRow( flightObj ) {
		var weekarr=[];
		$.each(flightObj.week, function(ind, val) {
			if(val.length>1)
				weekarr.push(weekObj[ind]+" "+val[1]+"; ");
		});
		return ['<td>',flightObj.flight,'</td>',
			'<td>',flightObj.route,'</td>',
			'<td>',$("select[name=flighttype] option[value='"+flightObj.flighttype+"']").text(),'</td>',
			'<td>',$("select[name=flightclass] option[value='"+flightObj.flightclass+"']").text(),'</td>',
			'<td>',weekarr.join(''),'</td>',
			'<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>',
			'<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>'].join('');
	}
    // init calendar
	function app() {
      $("[data-toggle=tooltip]").tooltip();
	  $('.timepicker').hide().timepicker({showMeridian: false, showInputs: false,  minuteStep: 5, defaultTime: '00:00'});
	  $("#flightform").on("submit", function () {
        var allData = $("#flightform").serialize();
          $.ajax({
            url: "/manager/flights/savedata",
            data: allData,
            success: function (msg) {
               if (msg.state=='ok'){
					var formData=$('#flightform').serializeObject();
					if(msg.hasOwnProperty('newid') && msg.newid != null && msg.newid !== undefined){
						var addto = "#all_flights";
						var newIn = '<tr id="tr_'+msg.newid+'" data-id="'+msg.newid+'"></tr>';
						var row=$(newIn);
						$(addto).append(row);
					
					  }else{
							var row=$('#tr_'+formData.id);
					  } 
					  row.html(generateFlightRow(formData));
					  bootstrap_alert.success(msg.answer);
				  }else{
					bootstrap_alert.warning(msg.answer);
					//$('.btn-u').prop("disabled", false).children('i').removeClass('fa-spin fa-spinner').addClass('fa-user');
				}
            }
          });
		return false;
      });
	  $("#delform").on("submit", function () {
		var allData = $("#delform").serialize();
          $.ajax({
            url: "/manager/flights/deletedata",
            data: allData,
            success: function (msg) {
               if (msg.state=='ok'){
					var formData=$('#delform').serializeObject();
					var row=$('#tr_'+formData.id);
					  row.remove();
					  bootstrap_alert.success(msg.answer);
				  }else{
					bootstrap_alert.warning(msg.answer);
					//$('.btn-u').prop("disabled", false).children('i').removeClass('fa-spin fa-spinner').addClass('fa-user');
				}
            }
          });
		return false;
	  });
	  $('input[type=checkbox]').click(function ( event ) {
		$('.timepicker[name='+addslashes($(this).attr('name'))+']').toggle();
		
	  });
	  $('#edit').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  emptyForm();
		  if (button.data('title') !='Add') {
			  var flight_id = button.closest('tr').data('id'); // Extract info from data-* attributes
			  $.getJSON('/manager/flights/getdata',{id:flight_id})
				.done(function( msg ) {
					  if (msg.state=='ok'){
						$.each(msg.answer, function(index, value) {
							if(weekObj.hasOwnProperty(index) && value!=null)
							{
								$('input[type=checkbox][name=week\\['+index+'\\]\\[\\]]').prop('checked', 'checked');
								$('input[type=text][name=week\\['+index+'\\]\\[\\]]').show().timepicker('setTime', value);
							} else
								$('[name='+index+']').val(value);
							
						});
					  }
				});
			}
		});
	$('#delete').on('show.bs.modal', function (event) {
			emptyForm();
			var button = $(event.relatedTarget);
		    var flight_id = button.closest('tr').data('id'); // Extract info from data-* attributes
			$('#delete [name=id]').val(flight_id);
		});
    }
    // Return all functions
    return {
      init: function () {
        preloader()
		settings()
        placeholderIE()
		app()       
      }
    }
  }();

  $(function () {
    // Launch functions
    bEvent.init()
  })
}(window.jQuery);