/*!
 * event manager for calendar
 *
 * Author: yasinsky
 */
var EventManager = (function ($)
{
	
	var EventManager = {};
	var self = this;
	self.rootEventSource = [];
	self.filteredEventSource = [];
	self.$calendar = $("#calendar");
	
	// Usage: EventManager.setSource([pass,event,array,here]);
	EventManager.getEventsSource = function (id)
	{
		if(id)
		{
			for(var i in self.filteredEventSource)
			{
				if(self.filteredEventSource[i].id == id)
				{
					return self.filteredEventSource[i];
				}
			}
			return null;
		}
		return self.filteredEventSource;
	};
	EventManager.setRootSource = function (start, end)
	{
		sts = Math.round(start.getTime() / 1000);
		ets = Math.round(end.getTime() / 1000);
		$.getJSON(
			"/manager/calendar/schedule",
			{start: sts, end: ets}
		).done(function (msg)
		{
			//	rootEventSource=rootEventSource.concat(msg);
			$.each(msg, function (k, event)
			{
				gotit = false;
				$.each(self.rootEventSource, function (i, n)
				{
					if (n.id == event.id)
					{
						gotit = true;
						return false;
					}
				});
				if (!gotit) self.rootEventSource.push(event);
			});
			self.$calendar.fullCalendar('removeEventSource', self.filteredEventSource);

			self.filteredEventSource = $.grep(self.rootEventSource, function (event)
			{
				return true;
			});
			self.$calendar.fullCalendar('addEventSource', self.filteredEventSource);
		});
	};

	/*      filterCalendar : function(){

	 $("#calendar").fullCalendar('removeEventSource',filteredEventSource);

	 filteredEventSource = $.grep(rootEventSource, function(event){
	 return event.resid  == resourse_id;
	 });

	 $("#calendar").fullCalendar('addEventSource', filteredEventSource);
	 },

	 changeEvData : function(evid,name,value){
	 $.each(rootEventSource, function(i,n){
	 if(n.id==evid)  {rootEventSource[i][name]=value; return false;}
	 });
	 },

	 changeEvent : function(newEvent){
	 if(!newEvent.id) return false;
	 var gotit=false;
	 $.each(rootEventSource, function(i,n){
	 if(n.id==newEvent.id)  {
	 $.each(newEvent, function(ind,val){
	 rootEventSource[i][ind]=val;
	 });
	 gotit=true;
	 return false;
	 }
	 });
	 return gotit;
	 },

	 addEvent: function(event){
	 rootEventSource.push(event);
	 } */
	return EventManager;
}(window.jQuery));
