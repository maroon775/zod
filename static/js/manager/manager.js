!function ($)
{

	"use strict";

	var bEvent = function ()
	{

		var bootstrap_alert = function ()
		{
		}
		// Preloader
		function preloader()
		{
			$(window).load(function ()
			{
				$(".preloader").fadeOut();
				$("body").removeClass("remove-scroll");
			});
		}

		// A jQuery plugin that enables HTML5 placeholder behavior for browsers that aren’t trying hard enough yet
		function placeholderIE()
		{
			if ($.fn.placeholder)
			{
				$("input, textarea").placeholder()
			}
		}

		function settings()
		{
			window.oneHideAjaxPreloader = false;
			$.ajaxSetup({
				type      : "POST",
//				dataType: "json",
				beforeSend: function ()
				{
					if ((!this.data || !this.data.match(/(ajax\=(.*)-form)/)) && window.oneHideAjaxPreloader !== true)
					{
						$('.preloader').css('display', 'block');
					}
					window.oneHideAjaxPreloader = false;
				},
				complete  : function ()
				{
					$('.preloader').css('display', 'none');

//					Actions.update();
				},
				error     : function (XMLHTTPObj)
				{
					alert("Произошла ошибка при выполнении AJAX запроса, возможно страница не найдена. Ответ от сервера" + XMLHTTPObj.responseText);
				}
			});
			bootstrap_alert.warning = function (message)
			{
				$('.alert_placeholder').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> ' + message + '</div>')
			}
			bootstrap_alert.success = function (message)
			{
				$('.alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Cool!</strong> ' + message + '</div>')
			}
		}

		// init calendar
		function initCalendar()
		{
			var settings;
			var calendar = $('#calendar');
			$.getJSON('/manager/calendar/settings').success(function (jsonData)
			{
				settings = jsonData;

				calendar.fullCalendar({
					header      : {
						left  : 'prev,next today',
						center: 'title',
						right : 'month,agendaWeek,agendaDay'
					},
					height      : 625,
					timeFormat  : 'HH:mm',
					defaultView : 'month',
					selectable  : false,
					editable    : false,
					selectHelper: true,
					select      : function (start, end, allDay)
					{

					},
					dayClick    : function (eventDate)
					{
						if (settings.dayClickUrl)
						{
							var EventEditModal = $('#EventEditModal');
							$.ajax({
								url     : settings.dayClickUrl,
								data    : {'event_date': eventDate.getTime() / 1000},
								dataType: 'html',
								success : function (html)
								{
									$('#EventEditWrapper', EventEditModal).html(html);
									EventEditModal.modal('show');
								}
							});
						}
					},
					events      : function (start, end, callback)
					{
						EventManager.setRootSource(start, end);
						callback([]);
					},
					eventClick  : function (event)
					{
						var url = settings.eventClickUrl;
						var EventEditModal = $('#EventEditModal');
						$.ajax({
							url     : url.replace('_ID_', event.id),
							dataType: 'html',
							success : function (html)
							{
								$('#EventEditWrapper', EventEditModal).html(html);
								EventEditModal.modal('show');
							}
						});
					},
					eventDrop   : function (event, dayDelta, minuteDelta, allDay, revertFunc)
					{

					},
					eventResize : function (event, dayDelta, minuteDelta, revertFunc)
					{

					}
				});
			});
		}

		// Custom scripts
		function app()
		{
			Actions.init();
			$('#passangers').editableTableWidget();
			//$('#textAreaEditor').editableTableWidget({editor: $('<textarea>')});
			$('.table-add').click(function ()
			{
				var $clone = $('#passangers').find('tr.hide').clone(true).removeClass('hide table-line');
				$('#passangers').find('tbody').append($clone);
			});
			$('.table-remove').click(function ()
			{
				$(this).parents('tr').detach();
			});
			$('#fillschedule').click(function ()
			{
				$.ajax({
					dataType: 'json',
					url     : "/manager/calendar/fillSchedule",
					success : function (data)
					{
						if (data.state == 'ok')
						{
							$.notify('Done', {
								className     : "success",
								globalPosition: "bottom left",
								showAnimation : "fadeIn",
								hideAnimation : "fadeOut"
							});

							if (data.schedule)
							{
								$("#calendar").fullCalendar("addEventSource", data.schedule);
							}
						}
					}
				});
			});
			/*$.get('/manager/calendar/typeahead', function (data)
			 {
			 $(".typeahead").typeahead({source: data, minLength: 2});
			 }, 'json');*/
			$('.datepicker').datepicker();
			$('.timepicker').timepicker({showMeridian: false, showInputs: false, minuteStep: 5, defaultTime: '00:00'});
		}

		// Return all functions
		return {
			init: function ()
			{
				preloader()
				placeholderIE()
				settings()
				initCalendar()
				app()
			}
		}
	}();

	$(function ()
	{
		// Launch functions
		bEvent.init()
//		$('[data-toggle=tooltip]').tooltip();
	})
}(window.jQuery);
