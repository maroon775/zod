/*!
 * actions module for calendar page
 *
 * Author: yasinsky
 */
var Actions = (function ($)
{

	'use strict';
	
	var Actions = {};
	Actions.options = {
		element       : "#act_list",
		pageLimit     : 15,
		shortlistlimit: 3,
		tablename     : '#all_data'
	};
	var allActions = [];
	var lastupdate = 0;
	var page = 1;
	var template = ['<div class="feed-element">',
		'<div class="media-body ">',
		'<strong>{username}</strong> {action}. <br>',
		'<small class="text-muted">{actdate}</small>',
		'</div>',
		'</div>'].join('');
	var thead = ['<thead>',
		'<tr>',
		'<th>Дата</th>',
		'<th>Автор</th>',
		'<th>Действие</th>',
		'</tr>',
		'</thead>'].join('');
	var rowtemplate = ['<tr>',
		'<td>{actdate}</td>',
		'<td>{username}</td>',
		'<td>{action}</td>',
		'</tr>'].join('');
	var tablecaption = "Последние действия";
	
	var filltemplate = function (temp, obj)
	{
		for (var i in obj)
			temp = temp.split('{' + i + '}').join(obj[i]);

		return temp;
	};
	
	var updateActions = function ()
	{
		$.post('/manager/calendar/updateactions', {ludate: lastupdate}, function (data)
		{
			if (data.state == 'ok')
			{
				lastupdate = data.answer.ludate;
				$.each(data.answer.newacts, function (key, val)
				{
					addAction(val);
				});
				renderShortList();
			}
		}, 'json');
	};
	
	var renderShortList = function ()
	{
		var newel = $(Actions.options.element);
		newel.empty();
		var limit = Actions.options.shortlistlimit < allActions.length ? Actions.options.shortlistlimit : allActions.length;
		for (var i = 0; i < limit; i++)
			newel.append(filltemplate(template, allActions[i]));
		
	};
	
	var addAction = function (action)
	{
		allActions.unshift(action);
		var lengthdiff = Actions.options.pageLimit - allActions.length;
		if (lengthdiff < 0) allActions.splice(lengthdiff);
	};
	
	var initUpdate = function ()
	{
		
	};
	
	var showall = function ()
	{
		var table = $(Actions.options.tablename);
		table.empty();
		table.append(thead);
		var tbody = ['<tbody>'];
		$.each(allActions, function (key, val)
		{
			tbody.push(filltemplate(rowtemplate, val));
		});
		tbody.push('</tbody>');
		table.append(tbody.join(''));
		$('#table_caption').html(tablecaption);
	};
	
	Actions.init = function ()
	{
		$('#portfolioModal1').on('show.bs.modal', function (event)
		{
			var button = $(event.relatedTarget); // Button that triggered the modal
			if (button.attr('name') == 'act_button')
			{
				showall();
			}
		});
		updateActions();
	};
	Actions.update = function(){
		window.oneHideAjaxPreloader = true;
		updateActions();
	};
	
	return Actions;
}(window.jQuery));
