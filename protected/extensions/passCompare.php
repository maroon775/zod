<?php
	class passCompare extends CValidator
	{

		public $first;
		public $second;

		protected function validateAttribute($object,$attribute)
		{
			$first = $this->first;
			$second = $this->second;

			//if( $first == 'password' && $second == 'reppassword' )
			//{
				//file_put_contents('test.txt', $object->$first . ' - ' . $object->$second);
				if( $object->$first != $object->$second )
				{
					$this->addError($object, $attribute, Yii::t('main', 'Passwords do not match.'));
					return false;
				}
			//}
			return true;
		}

	}