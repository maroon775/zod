<?php
	class Pager extends CLinkPager
	{
		public $skipCenter = false;
		/**
		 * Creates the page buttons.
		 * @return array a list of page buttons (in HTML code).
		 */
		protected function createPageButtons()
		{
			if(($pageCount=$this->getPageCount())<=1)
				return array();

			list($beginPage,$endPage)=$this->getPageRange();
			$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
			$buttons=array();

			// prev page
			if(($page=$currentPage-1)<0)
				$page=0;
			$buttons[]=$this->createPageButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0,false);

			/*// internal pages
			for($i=$beginPage;$i<=$endPage;++$i)
				$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);*/
			if( !$this->skipCenter )
				$buttons[] = '<li><a>' . Yii::t('main', '{current} of {total}', array('{current}' => $this->currentPage+1, '{total}' => $this->pageCount)) . '</a></li>';

			// next page
			if(($page=$currentPage+1)>=$pageCount-1)
				$page=$pageCount-1;
			$buttons[]=$this->createPageButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1,false);


			return $buttons;
		}

		/**
		 * Executes the widget.
		 * This overrides the parent implementation by displaying the generated page buttons.
		 */
		public function run()
		{
			$this->registerClientScript();
			$buttons=$this->createPageButtons();
			if(empty($buttons))
				return;
			echo $this->header;
			echo CHtml::tag('div', array('class' => 'display-inline'), CHtml::tag('ul',$this->htmlOptions,implode("\n",$buttons)));
			echo $this->footer;
		}
	}