<?
return array(
	'guest'         => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule'     => null,
		'data'        => null
	),
	'user'          => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'User',
		'children'    => array(
			'guest', // унаследуемся от гостя
		),
		'bizRule'     => null,
		'data'        => null
	),
	'contractor'    => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Contractor',
		'children'    => array(
			'user',
		),
		'bizRule'     => null,
		'data'        => null
	),
	'employee'       => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Employee',
		'children'    => array(
			'contractor',
		),
		'bizRule'     => null,
		'data'        => null
	),
	'manager'       => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Manager',
		'children'    => array(
			'employee',
		),
		'bizRule'     => null,
		'data'        => null
	),
	'administrator' => array(
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Administrator',
		'children'    => array(
			'manager',         // позволим админу всё, что позволено менеджеру
		),
		'bizRule'     => null,
		'data'        => null
	),
);
?>
