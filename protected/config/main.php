<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
/*$this->setComponents(array(

));*/
return array(
	'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'              => 'Yii ЗОД - Манас',
	'defaultController' => 'site/index',

	// preloading 'log' component
	'preload'           => array('log', 'booster'),

	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
	),

	'modules'           => array(
		// uncomment the following to enable the Gii tool
		'gii' => array(
			'class'          => 'system.gii.GiiModule',
			'password'       => false,
			'generatorPaths' => array(
				'booster.gii'
			),
			'ipFilters'      => array(/*'212.96.64.*/)
		),
		'manager'
	),

	'sourceLanguage'    => 'en',
	'language'          => 'ru',

	// application components
	'components'        => array(

		'messages'     => array(
			'class'           => 'CPhpMessageSource',
			'cachingDuration' => YII_DEBUG ? 0 : 7200
		),
		'coreMessages' => array(
			'basePath'        => Yiibase::getPathOfAlias('system.messages'),
			'class'           => 'CPhpMessageSource',
			'cachingDuration' => YII_DEBUG ? 0 : 7200
		),
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'class'          => 'BaseUser',
		),
		'booster'      => array(
			'class'        => 'ext.booster.components.Booster',
			'bootstrapCss' => true,
			'enableJS'     => true,
			'coreCss'      => true,
		),
		'image'        => array(
			'class' => 'ext.EPhpThumb.EPhpThumb',
		),

		'authManager'  => array(
			'class'        => 'PhpAuthManager',
			// Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
			'defaultRoles' => array('guest'),
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'   => array(
			'class'          => 'application.components.UrlManager',
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'caseSensitive'  => true,
			'rules'          => array_merge(
				require './protected/config/rules.php',
				require './protected/modules/manager/config/rules.php',
				array(
					'<module:\w+>'                                        => '<module>',
					'<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
					'<module:\w+>/<controller:\w+>/<action:\w+>/*'        => '<module>/<controller>/<action>',
					'<controller:\w+>/<id:\d+>'                           => '<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'              => '<controller>/<action>',
					'<controller:\w+>/<action:\w+>'                       => '<controller>/<action>',
					'<controller:\w+>'                                    => '<controller>/index',
				)
			),
		),

		/*
		'log'              => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'     => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
					// Access is restricted by default to the localhost
					'ipFilters' => array( ),
					'enabled'   => true
				),
				array(
					'class'   => 'CFileLogRoute',
					'enabled' => true
				),
				array(
					'class'         => 'CWebLogRoute',
					'categories'    => 'system.db.CDbCommand',
					'levels'        => 'error, warning, trace, profile, info',
					'showInFireBug' => true,
					'enabled'       => true,
				),
			),
		),*/
		'cache'        => array(
			'class' => 'CFileCache',
			/*'class'        => 'system.caching.CMemCache',
			'servers'      => array(
				array('host' => '127.0.0.1', 'port' => 11211, 'weight' => 512),
			),
			'useMemcached' => false,*/
		),

		// database settings are configured in database.php
		'db'           => require(dirname(__FILE__) . '/database.php'),

		'errorHandler' => array(
			// use 'site/error' action to display errors
//			'errorAction'=>'site/error',
		),


		'mail'         => array(
			'class'   => 'ext.mailer.EMailer',
			'CharSet' => 'UTF-8',
		),

		'clientScript' => array(
			"class"     => "EClientScript",
			'scriptMap' => array(
//				'jquery.js' => false,
//				'jquery'    => false,
			)
		),
	),

	// application-level parameters that can be accessed
	'params'            => array(
		// this is used in contact page
		'adminEmail'     => 'webmaster@example.com',

		'cacheTimeBase'  => YII_DEBUG ? 0 : 300,
		'cacheTimeLong'  => YII_DEBUG ? 0 : 3600,
		'ckeditorConfig' => "js:[
			    { name: 'document', items : [ 'NewPage','Preview' ] },
			    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
			    { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
			    { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
			    { name: 'styles', items : [ 'Styles','Format' ] },
			    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
			    { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
			    { name: 'tools', items : [ 'Maximize' ] },
			    { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ,'Iframe','-','About' ] }
			]",
	),
);
