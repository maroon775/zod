<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Configurator',

	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
		'application.modules.manager.*',
		'application.modules.manager.models.*',
	),
	
	'modules'           => array(
		'manager'
	),
	// preloading 'log' component
	'preload'=>array('log'),

	
	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),
);
