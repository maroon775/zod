<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main', 'Authorization');
?>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 offset-top">
			<div class="reg-page">
				<? $form = $this->beginWidget('CActiveForm', array(
					'id'                     => 'loginForm',
					'enableClientValidation' => false,
					'enableAjaxValidation'   => true,
					'clientOptions'          => array(
						'validateOnSubmit' => true,
						'afterValidate'=>"js:function(){

							console.log('execute - after validate callback');
							if('bAuth' in  window)
							{
								console.log('logging - before bAuth.login');
								return bAuth.login();
							}
						}"
					),
				)); ?>
				<h3>Вход в систему</h3>
				<br/>

				<div class="form-group">
					<?= $form->textField($model, 'username', array('class' => 'form-control input-lg', 'placeholder' => 'email или логин')); ?>
					<?= $form->error($model, 'username', array('class' => 'text-danger')); ?>
				</div>
				<div class="form-group">
					<?= $form->passwordField($model, 'password', array('class' => 'form-control input-lg', 'placeholder' => 'пароль')); ?>
					<?= $form->error($model, 'password', array('class' => 'text-danger')); ?>
				</div>

				<?= CHtml::submitButton(Yii::t('main', 'Login'), array('class' => 'btn btn-primary btn-block btn-lg')); ?>
				<br/>
				<h4>Забыли пароль?</h4>
				<p>Без паники,
					<a class="color-green" id="forgot_link" href="#">нажмите сюда</a>
				   , чтобы получить новый.
				</p>
				<div class="alert_placeholder"></div>
				<? $this->endWidget() ?>
			</div>
		</div>
	</div>
</div>
