<!-- ///  begin content  /// -->
<!-- ///  begin promo  /// -->
<div class="promo" id="home" style="background-image:url(static/img/bg/promo.jpg);">
	<!-- ///  semitransparent layer  /// -->
	<div class="color-correction"></div>
	<!-- ///  begin entry  /// -->
	<div class="container">
		<div class="row">
			<!-- ///  begin text section  /// -->
			<div class="col-md-6 wow fadeInUp" data-wow-duration="2s">
				<div class="promo-text">
					<h1>
						<i class="fa fa-signal success-color"></i>
						Зал Официальных Делегаций
					</h1>
					<hr>
					<p class="lead">Мы экономим временя официальных лиц на прохождение предполетных процедур. Для этого
					                у нас организован ускоренный режим таможенного, санитарного и паспортного контроля
					</p>
					<p style="font-size:16px;"> Кроме того, предоставляются различные сервисы: кабельное телевидение,
					                            интернет, свежая пресса, доставка на специальном транспорте до борта
					                            самолета и обратно
					</p>
				</div>
			</div>
			<!-- ///  end text section  /// -->
			<!-- ///  begin registration form  /// -->
			<div class="col-md-4 col-md-offset-2 wow fadeInDown" data-wow-duration="2s">
				<? $this->renderPartial('order') ?>
			</div>
			<!-- ///  end registration form  /// -->
		</div>
	</div>
	<!-- ///  end entry  /// -->
</div>
<!-- ///  end promo  /// -->

<!-- ///  begin schedule  /// -->
<div class="schedule" id="schedule">
	<div class="container">
		<div class="section-heading">
			<h1>Расписание</h1>
			<p class="lead">ближайших рейсов, возможных для бронирования</p>
		</div>
		<div class="row">
			<div class="col-md-12 wow fadeInDown">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
					<?
					$i = 1;
					foreach ($schedule as $k => $item): ?>
						<li <?=$i===1?'class="active"':''?>>
							<a href="#<?=date('dmY',$item[0]['fldate'])?>_day" data-toggle="tab"><?=$k?></a>
						</li>
						<?$i++?>
					<? endforeach ?>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<?
					$i = 1;
					foreach ($schedule as $k => $items): ?>
						<div class="tab-pane <?=$i===1?'active':''?>" id="<?=date('dmY',$items[0]['fldate'])?>_day">
							<? foreach ($items as $k=>$schedule):?>
							<div class="panel-group" id="accordion<?=$schedule->id?>">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion<?=$schedule->id?>" href="#collapse<?=$schedule->id?>">
												<?=$schedule->getDatetime('H:i') ?> <?=$schedule->flight->route?>
											</a>
										</h4>
									</div>
									<div id="collapse<?=$schedule->id?>" class="panel-collapse collapse">
										<div class="panel-body">
											<h4>Dolor Ipsum Solor</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, fuga
											   delectus nam quia omnis accusamus autem temporibus iure cum voluptatem rerum
											   neque quisquam sed provident similique quis natus fugiat id.
											</p>
										</div>
									</div>
								</div>
							</div>
							<?endforeach ?>
						</div>
						<?$i++?>
					<?endforeach ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ///  end schedule  /// -->

<!-- ///  begin video  /// -->
<div class="multimedia" id="multimedia">
	<div class="container">
		<div class="row mt20">
			<div class="col-md-6 wow fadeInLeft">
				<div class="video">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/IDmVpOzNnRg" frameborder="0" allowfullscreen></iframe>
					<!-- <iframe src="//player.vimeo.com/video/77790506?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
				</div>
			</div>
			<div class="col-md-6 wow fadeInRight">
				<h1>Видео</h1>
				<p class="lead">Международный Аэропорт Манас</p>
				<hr>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, ea, illum, consequatur, sunt
				   rem unde odio debitis veritatis porro dolores ipsam repudiandae officia praesentium vitae cupiditate
				   perferendis possimus laboriosam ducimus.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- ///  end video  /// -->

<!-- ///  begin photos  /// -->
<div class="photo">
	<div class="container">
		<div class="row mt20">
			<div class="col-md-6 wow fadeInLeft">
				<h1>Последние фото</h1>
				<p class="lead">lorem ipsum</p>
				<hr>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, ea, illum, consequatur, sunt
				   rem unde odio debitis veritatis porro dolores ipsam repudiandae officia praesentium vitae cupiditate
				   perferendis possimus laboriosam ducimus.
				</p>
			</div>
			<div class="col-md-6 wow fadeInRight">
				<div class="photo-wrapper">
					<div id="photoSlider" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active">
								<img src="<?= Yii::app()->request->baseUrl ?>static/img/carousel/slide1.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= Yii::app()->request->baseUrl ?>static/img/carousel/slide2.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= Yii::app()->request->baseUrl ?>static/img/carousel/slide3.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= Yii::app()->request->baseUrl ?>static/img/carousel/slide5.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="text-center">
					<div class="btn-group">
						<a href="#photoSlider" data-slide="prev" class="btn btn-success">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a href="#photoSlider" data-slide="next" class="btn btn-success">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ///  end photos  /// -->

<!-- ///  begin features  /// -->
<div class="theses" id="features">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-3 wow bounceIn" data-wow-delay=".4s">
				<div class="icon-wrap">
					<i class="icon-graduation-cap fa-3x"></i>
				</div>
				<h3>Ожидание</h3>
				<hr>
				<p>авиарейса в уютном зале, оборудованном кондиционерами, телевизионной техникой, беспроводным
				   интернетом, свежей прессой, богатым выбором спиртных и прохладительных напитков и закусок в баре,
				   комната отдыха для пассажиров
				</p>
			</div>
			<div class="col-md-3 wow bounceIn" data-wow-delay=".6s">
				<div class="icon-wrap">
					<i class="icon-lightbulb fa-3x"></i>
				</div>
				<h3>Организация</h3>
				<hr>
				<p>пресс-конференций, банкетов, услуги баров, встречу пассажиров девушками в национальных костюмах с
				   цветами, красной дорожкой (согласно прейскуранту)
				</p>
			</div>
			<div class="col-md-3 wow bounceIn" data-wow-delay=".8s">
				<div class="icon-wrap">
					<i class="icon-videocam fa-3x"></i>
				</div>
				<h3>Прохождение</h3>
				<hr>
				<p>паспортного и таможенного контроля через специально оборудованные пункты непосредственно в Зале.
				   Оформление авиаперевозочных документов и регистрация на рейс высококвалифицированным персоналом со
				   знанием иностранных языков
				</p>
			</div>
			<div class="col-md-3 wow bounceIn" data-wow-delay="1s">
				<div class="icon-wrap">
					<i class="icon-paper-plane fa-3x"></i>
				</div>
				<h3>Доставка</h3>
				<hr>
				<p>пассажиров и ручной клади от / до самолета автотранспортом повышенной комфортности</p>
			</div>
		</div>
	</div>
</div>
<!-- ///  end features  /// -->

<!-- ///  begin speakers  /// -->
<div class="speakers" id="speakers">
	<div class="container">
		<div class="section-heading">
			<h1>Персонал</h1>
			<p class="lead">Гарантия улыбки наших сотрудников</p>
		</div>
		<div class="row">
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/1.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>Tyler Durden</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1.1s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/2.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>Harry Callahan</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1.2s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/3.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>Amelie Poulain</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/4.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>Mal Reynolds</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1.1s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/5.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>Samantha Baker</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
			<div class="col-md-4 speaker wow fadeInDown" data-wow-delay="1.2s">
				<div class="speaker-img">
					<img src="<?= Yii::app()->request->baseUrl ?>/static/img/speakers/6.jpg" alt="">
					<a href="" class="speaker-contacts left-top">
						<i class="fa fa-google-plus fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts left-bottom">
						<i class="fa fa-facebook fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-bottom">
						<i class="fa fa-twitter fa-2x"></i>
					</a>
					<a href="" class="speaker-contacts right-top">
						<i class="fa fa-envelope fa-2x"></i>
					</a>
				</div>
				<div class="speaker-desc">
					<p>George Bailey</p>
					<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ///  end speakers  /// -->

<!-- ///  begin testimonials  /// -->
<div class="testimonials wow flipInX" style="background-image: url(<?= Yii::app()->request->baseUrl ?>/static/img/bg/smiling-portraits.jpg)">
	<div class="color-correction"></div>
	<div class="container">
		<div class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<div class="carousel-caption">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
								   ante.
								</p>
								<footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
							</blockquote>
						</div>
					</div>
					<div class="item">
						<div class="carousel-caption">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
								   ante.
								</p>
								<footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
							</blockquote>
						</div>
					</div>
					<div class="item">
						<div class="carousel-caption">
							<blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
								   ante.
								</p>
								<footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
							</blockquote>
						</div>
					</div>
				</div>
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- ///  end testimonials  /// -->

<!-- ///  begin price  /// -->
<div class="price" id="price">
	<div class="container">
		<div class="section-heading">
			<h1>Цены</h1>
		</div>
		<div class="row">
			<div class="col-sm-3 info-bg wow fadeInDown" data-wow-delay=".4s">
				<header class="price-header">
					<h4>Внутренний</h4>
					<p>1000 Сом</p>
				</header>
				<ul class="list-group">
					<li class="list-group-item">
						<p class="pull-left">One day success</p>
						<p class="pull-right">
							<i class="fa fa-check primary-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Coffee Break</p>
						<p class="pull-right">
							<i class="fa fa-check primary-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Seat</p>
						<p class="pull-right">
							<i class="fa fa-check primary-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Certificate</p>
						<p class="pull-right">
							<i class="fa fa-check primary-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Lorem</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Ipsum</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Dolor</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Sit</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-3 success-bg wow fadeInUp" data-wow-delay=".8s">
				<header class="price-header">
					<h4>Международный</h4>
					<p>3000 Сом</p>
				</header>
				<ul class="list-group">
					<li class="list-group-item">
						<p class="pull-left">One day success</p>
						<p class="pull-right">
							<i class="fa fa-check success-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Coffee Break</p>
						<p class="pull-right">
							<i class="fa fa-check success-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Seat</p>
						<p class="pull-right">
							<i class="fa fa-check success-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Certificate</p>
						<p class="pull-right">
							<i class="fa fa-check success-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Lorem</p>
						<p class="pull-right">
							<i class="fa fa-check success-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Ipsum</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Dolor</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Sit</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-3 danger-bg wow fadeInDown" data-wow-delay="1.2s">
				<header class="price-header">
					<h4>Дети до 12ти лет</h4>
					<p>50%</p>
				</header>
				<ul class="list-group">
					<li class="list-group-item">
						<p class="pull-left">One day success</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Coffee Break</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Seat</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Certificate</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Lorem</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Ipsum</p>
						<p class="pull-right">
							<i class="fa fa-check danger-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Dolor</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Sit</p>
						<p class="pull-right">
							<i class="fa fa-minus text-muted fa-lg"></i>
						</p>
					</li>
				</ul>
			</div>
			<div class="col-sm-3 warning-bg wow fadeInUp" data-wow-delay="1.6s">
				<header class="price-header">
					<h4>Дети до 2х лет</h4>
					<p>0 Сом</p>
				</header>
				<ul class="list-group">
					<li class="list-group-item">
						<p class="pull-left">One day success</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Coffee Break</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Seat</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Certificate</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Lorem</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Ipsum</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Dolor</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
					<li class="list-group-item">
						<p class="pull-left">Sit</p>
						<p class="pull-right">
							<i class="fa fa-check warning-color fa-lg"></i>
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- ///  end price  /// -->

<!-- ///  begin faq  /// -->
<div class="faq" id="faqSection">
	<div class="container">
		<div class="row">
			<div class="col-md-4 wow fadeInLeft">
				<h1>Вопросы</h1>
				<p class="lead">часто задаваемые вопросы</p>
				<hr>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, officiis, nisi temporibus
				   reiciendis possimus inventore fugit. Natus, omnis, earum consectetur aperiam harum sequi qui soluta
				   deserunt provident maxime voluptate dolores.
				</p>
			</div>
			<div class="col-md-8 wow fadeInRight">
				<div class="panel-group" id="faq">
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq1">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq1" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq2">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq2" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq3">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq3" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq4" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq5">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq5" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
					<div class="panel">
						<a data-toggle="collapse" data-parent="#faq" href="#faq6">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit?
						</a>
						<div id="faq6" class="panel-collapse collapse">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, perferendis facere non
							   laudantium nostrum odit corporis repudiandae voluptas dolorem consequuntur! Assumenda,
							   eveniet, voluptates quos id at facilis perferendis obcaecati recusandae?
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ///  end faq  /// -->

<!-- ///  begin partners  /// -->
<div class="partners wow fadeInUp">
	<div class="container">
		<div class="section-heading">
			<h1>Partners</h1>
			<p class="lead">Lorem ipsum dolor sit amet</p>
		</div>
		<div class="row text-center">
			<div class="col-lg-12">
				<ul class="list-inline">
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/facebook.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/magento.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/twitter.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/wordpress.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/facebook.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/magento.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/twitter.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/wordpress.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/facebook.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/magento.png" alt="">
					</li>
					<li>
						<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/partners/facebook.png" alt="">
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- ///  end partners  /// -->

<!-- ///  begin map, subscribe, contacts  /// -->
<!-- ///  set your venue address in data-location attribute  /// -->
<div class="map" id="ourLocation">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 map-container" id="map">
			</div>
		</div>
	</div>
</div>
<!-- ///  end map, subscribe, contacts  /// -->
<!-- ///  end content  /// -->
