<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('main','Error');;
$this->breadcrumbs=array(
	Yii::t('main','Error'),
);
?>

<h2><?=Yii::t('main','Error');?> <?=$code;?></h2>

<div class="error"><?=CHtml::encode($message); ?></div>
