<? /**
 *
 * @var $this SiteController
 * @var $form TbActiveForm
 * @var $model Order
 *
 */
$model = new Order;
?>
<div class="register">
	<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'                   => 'registrationForm',
		'enableAjaxValidation' => true,
		'htmlOptions'          => array(
			'action' => $this->createUrl('order'),
		)
	));
	?>
	<div class="form-group">
		<?= $form->textField($model, 'fio', array('class' => 'form-control input-lg', 'placeholder' => $model->attributeLabels()['fio'])) ?>
	</div>
	<div class="form-group">
		<?= $form->textField($model, 'email', array('class' => 'form-control input-lg', 'placeholder' => $model->attributeLabels()['email'])) ?>
	</div>
	<div class="form-group">
		<?= $form->textField($model, 'phone', array('class' => 'form-control input-lg', 'placeholder' => $model->attributeLabels()['phone'])) ?>
	</div>
	<div class="form-group">
		<?= $form->textField($model, 'passport', array('class' => 'form-control input-lg', 'placeholder' => $model->attributeLabels()['passport'])) ?>
	</div>
	<div class="form-group">
		<?= $form->dropDownList(
			$model,
			'schedule_id',
			$this->scheduleList(),
			array(
				'class' => 'form-control btn-group-lg selectpicker',
				'options'=> array(
					0 => array('disabled'=>true,'selected'=>true)
				)
			)
		) ?>
	</div>
	<button type="submit" class="btn btn-empty btn-block btn-lg">Бронировать</button>
	<? $this->endWidget() ?>
	<div class="ok">
		<p class="icon">
			<i class="fa-check-circle-o fa fa-5x success-color"></i>
		</p>
		<h3>Thanks!</h3>
		<p class="lead">Register complete</p>
		<button type="button" class="btn btn-warning" id="back">Back</button>
	</div>
</div>
