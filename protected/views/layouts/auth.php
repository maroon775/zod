<?php /**
 * @var $this SiteController
 * @var $content string
 */
//Yii::app()->clientScript->registerCoreScript("jquery");
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="icon" href="<?= Yii::app()->request->baseUrl; ?>/favicon.ico">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/fonts/fontawesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/fonts/linecons/css/linecons.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/css/plugins/animate/animate.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/css/plugins/select/bootstrap-select.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/bootstrap-3.1.1/css/bootstrap.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/static/css/main.css"/>
	<title><?= CHtml::encode($this->pageTitle); ?></title>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="remove-scroll">
<div class="preloader">
	<img src="<?= Yii::app()->request->baseUrl; ?>/static/img/loader.gif" class="loader" alt="">
</div>
<!-- ///  begin header  /// -->
<header class="main-header">
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">
					<i class="fa fa-signal success-color"></i>
					ЗОД -
					<b>Манас</b>
				</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right" id="nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Меню
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<?php
							if (Yii::app()->user->isManager)
							{
								$submenu = array(
									array('link' => '/manager', 'name' => 'Календарь', 'icon' => '<i class="fa fa-calendar"></i>'),
									array('link' => '/manager/flights', 'name' => 'Расписание рейсов', 'icon' => '<i class="fa fa-calendar"></i>'),
								);
								foreach ($submenu as $sub)
								{
									if ($sub['icon'] == 'devider')
									{
										echo '<li role="presentation" class="divider"></li>
											<li role="presentation" class="dropdown-header">' . $sub['name'] . '</li>';
									} else
										echo '<li><a href="' . $sub['link'] . '">' . $sub['icon'] . ' ' . $sub['name'] . '</a></li>';
								}
							}
							?>
							<li class="divider"></li>
							<li>
								<a href="/auth/logout">
									<i class="fa fa-power-off"></i>
									Выход
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
</header>
<!-- ///  end header  /// -->

<div id="b-main_content">
	<?= $content ?>
</div>

</body>
</html>
