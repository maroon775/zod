<?php

class ManagerModule extends CWebModule
{
	public function init()
	{
		Yii::setPathOfAlias('manager', Yii::getPathOfAlias('application.modules.manager'));
		$this->layoutPath = Yii::getPathOfAlias('manager.views.layouts');
		// set the layout
		$this->layout = 'main';
		// this method is called when the module is being created

		$this->setComponents(array(
			'errorHandler' => array(
				'errorAction' => '/default/error'
			),
			'authManager'  => array(
				'class'        => 'application.components.PhpAuthManager',
				'defaultRoles' => array('guest'),
			),
			'clientScript' => array(
				"class"     => "application.components.EClientScript",
				'scriptMap' => array(
//					'jquery.js' => false,
//					'jquery'    => false,
				)
			)
		));

		// import the module-level models and components
		$this->setImport(array(
			'manager.models.*',
			'manager.components.*',
			'manager.messages.*',
		));


		if (!Yii::app()->request->isAjaxRequest)
		{
			$assetUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.manager.assets'));
			Yii::app()->clientScript->registerScriptFile($assetUrl . '/js/manager.js');
			Yii::app()->clientScript->registerCssFile($assetUrl . '/css/manager.css');
		}

		$this->defaultController = 'calendar/index';

		Yii::app()->language = 'ru';
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			$route = $controller->id . '/' . $action->id;
			$publicPages = array(
				'default/login',
				'default/error',
			);
			if (!$this->hasAccessToModule() && !in_array($route, $publicPages))
			{
				Yii::app()->user->loginUrl = array('/site/auth');
				Yii::app()->user->loginRequired();
			}

			return true;
		} else
			return false;
	}

	public function hasAccessToModule()
	{
		if (Yii::app()->user->isEmployee || Yii::app()->user->isContractor)
		{
			return true;
		}

		return false;
	}

}
