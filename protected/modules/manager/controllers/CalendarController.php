<?

class CalendarController extends MainController
{
	/*public function actionTypeahead()
	{
		$data = Flights::model()->findAll();
		$arResult = array();

		foreach ($data as $item)
		{
			$arResult[] = $item->flight;
		}
		echo CJSON::encode($arResult);
		Yii::app()->end();
	}*/

	public function actionUpdateActions()
	{
		$result = array('state' => 'ok', 'answer' => '');

		if (isset($_POST['ludate']))
		{
			$result['answer']['ludate'] = time();

			$criteria = new CDbCriteria();
			$criteria->addCondition('t.actdate >=' . Yii::app()->db->quoteValue($_POST['ludate']));
			$criteria->addCondition('t.user_id =' . Yii::app()->user->id);
			$criteria->order = "t.`actdate` ASC";
			$criteria->limit = 15;

			$actions = Actions::model()->findAll($criteria);

			foreach ($actions as $action)
			{
				$result['answer']['newacts'][] = array(
					'actdate'  => date("d.m.Y H:i", $action->actdate),
					'action'   => $action->action,
					'id'       => $action->id,
					'username' => $action->user->name,
				);
			}

			echo CJSON::encode($result);
			Yii::app()->end();
		}
	}

	public function actionSchedule()
	{
		$criteria = new CDbCriteria();

		if (!empty($_POST['start']) && !empty($_POST['end']))
		{
			$criteria->addBetweenCondition('t.fldate', $_POST['start'], $_POST['end']);
		}

		$data = Schedule::model()->findAll($criteria);
		$json = array();

		foreach ($data as $item)
		{
			$json[] = array(
				'id'     => $item->id,
				'title'  => $item->flight->flight,
				'start'  => $item->datetime,
				'allDay' => false
			);
		}

		echo CJSON::encode($json);
		Yii::app()->end();
	}

	public function actionFillSchedule()
	{
		$days       = date('t');
		$year       = date('Y');
		$month      = date('m');
		$today      = date('j');
		$arSchedule = array();
		$insert     = array();


		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition(
			'fldate',
			strtotime(date('Y-m-01 00:00:00')),
			strtotime(date('Y-m-' . $days . ' 23:59:59'))
		);

		$data = Schedule::model()->findAll($criteria);

		foreach ($data as $schedule)
		{
			$arSchedule[date('j', $schedule->fldate)][$schedule->flight_id] = true;
		}

		/* flights */
		$criteria = new CDbCriteria();
		$criteria
			->addCondition('`mon` IS NOT NULL', 'OR')
			->addCondition('`tue` IS NOT NULL', 'OR')
			->addCondition('`wed` IS NOT NULL', 'OR')
			->addCondition('`thu` IS NOT NULL', 'OR')
			->addCondition('`fri` IS NOT NULL', 'OR')
			->addCondition('`sat` IS NOT NULL', 'OR')
			->addCondition('`sun` IS NOT NULL', 'OR');

		$flights = Flights::model()->findAll($criteria);

		for ($day = $today; $day <= $days; $day++)
		{
			$daystamp = mktime(0, 0, 0, $month, $day, $year);
			$dayName  = strtolower(date('D', $daystamp));

			foreach ($flights as $flight)
			{
				// проспускаем процесс если есть рейс на $day(день) месяца или день позже сегодняшнего
				if (!empty($arSchedule[$day][$flight->id])) continue;

				// если нет то нужно заполнить значением времени полета если оно есть
				if ($flight->{$dayName} != null)
				{
					$model            = new Schedule();
					$model->flight_id = $flight->id;
					$model->fldate    = strtotime(date("Y-m-d", $daystamp) . " " . $flight->{$dayName});
					$model->save();

					$resultData[] = array(
						'id'     => $model->id,
						'title'  => $model->flight->flight,
						'start'  => $model->datetime,
						'allDay' => false
					);
				}
			}
		}

		if (!empty($insert))
			Yii::app()->db->createCommand("
				INSERT INTO `schedule` (`flight_id`, `fldate`) VALUES " . implode(',', $insert) . "
			")->execute();

		echo CJSON::encode(array('state' => 'ok', 'schedule' => $resultData));
		Yii::app()->end();
	}

	public function actionIndex()
	{
		echo Yii::app()->errorHandler->errorAction;
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/modernizr-latest.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/jquery-ui-1.10.3.custom.min.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/datepicker/bootstrap-datepicker.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/timepicker/bootstrap-timepicker.min.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/fullcalendar/fullcalendar.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/actions/actions.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/editable/editable.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/typeahead/typeahead.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/manager/eventmanager.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/manager/manager.js', CClientScript::POS_END);

		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/fonts/fontawesome/css/font-awesome.min.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/fonts/linecons/css/linecons.css');
		Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/bootstrap-3.1.1/css/bootstrap.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/plugins/datepicker/datepicker.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/navbar.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/fullmodal.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/style.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/actions.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/plugins/fullcalendar/fullcalendar.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/plugins/editable/editable.css');


		Yii::import('booster.components.Booster');
		Booster::getBooster()->registerPackage('select2');


		$criteria = new CDbCriteria();
		if (Yii::app()->user->role == 'contractor')
		{
			$criteria->addCondition('t.contractor_id = ' . Yii::app()->user->id);
		}
		$criteria->order = 't.order_date DESC';
		$criteria->limit = 3;
		$orders          = Order::model()->findAll($criteria);

		$this->render('index', array('orders' => $orders));
	}


	public function actionSettings()
	{
		$json = array();

		switch (Yii::app()->user->role)
		{
			case 'manager':
				$json = array(
					'eventClickUrl' => $this->createUrl('/manager/schedule/update/', array('id' => "_ID_")),
					'dayClickUrl'   => $this->createUrl('/manager/schedule/create/'),
				);
				break;
			case 'contractor':
				$json = array(
					'eventClickUrl' => $this->createUrl('/manager/order/create/', array('schedule_id' => "_ID_")),
					'dayClickUrl'   => 0,
				);
				break;
			case 'employee':
				$json = array(
					'eventClickUrl' => $this->createUrl('/manager/schedule/update/', array('id' => "_ID_")),
					'dayClickUrl'   => 0,
				);
				break;
			default:
				$json = array(
					'eventClickUrl' => $this->createUrl('/manager/schedule/update/', array('id' => "_ID_")),
					'dayClickUrl'   => $this->createUrl('/manager/schedule/create/'),
				);
		}
		echo CJSON::encode($json);
		Yii::app()->end();
	}
}

?>
