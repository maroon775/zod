<?php

class ScheduleController extends MainController
{

	public function actionFill()
	{
		$model = Schedule::model()->find();
		ORM::factory('Schedule')->fillschedule();
		$this->response->body(json_encode(array('state' => 'ok')));
	}

	public function actionCreate()
	{
		$this->actionUpdate();
	}

	public function actionUpdate($id = null)
	{

		$model = $id === null ? new Schedule() : $this->loadModel($id);
		$action = $id === null ? 'create' : 'update';


		$this->performAjaxValidation($model);

		if (!empty($_POST['Schedule']))
		{
			$model->attributes = $_POST['Schedule'];
			if ($model->isNewRecord)
			{
				$json['hasEventAdd'] = true;
				$json['notifyMessage'] = Yii::t('ManagerModule.main', 'Schedule added successfully');
				$json['event'][] = array(
					'title'  => $model->flight->flight,
					'start'  => $model->getDatetime('Y-m-d H:i:s'),
					'allDay' => false
				);
			} else
			{
				$json['hasEventUpdate'] = true;
				$json['notifyMessage'] = Yii::t('ManagerModule.main', 'Schedule updated successfully');
				$json['event'] = array(
					'id'     => $model->id,
					'title'  => $model->flight->flight,
					'start'  => $model->getDatetime('Y-m-d H:i:s'),
					'allDay' => false
				);
			}

			if ($model->save() && Yii::app()->request->isAjaxRequest)
			{
				$this->logAction();
				$json['event'][0]['id'] = $model->id;
				echo CJSON::encode($json);
				Yii::app()->end();
			}
		}

		if ($action == 'create' && !empty($_REQUEST['event_date']) && $model->isNewRecord)
		{
			$model->datetime = date('Y-m-d H:i', $_REQUEST['event_date']);
		}
		elseif ($action == 'update')
		{
			$model->datetime = date('Y-m-d H:i', $model->fldate);
		}


		Yii::app()->clientScript->registerCoreScript('yiiactiveform');

		if (Yii::app()->request->isAjaxRequest)
		{

			Yii::app()->clientScript->scriptMap['bbq'] = false;
			Yii::app()->clientScript->packages['bbq'] = false;
			Yii::app()->clientScript->corePackages['bbq'] = false;
			Yii::app()->clientScript->packages['select2'] = false;
			Yii::app()->clientScript->packages['bootstrap.js'] = false;
			Yii::app()->clientScript->clearJquery();

			$this->renderPartial($action, array('model' => $model), false, true);
		} else
			$this->render($action, array('model' => $model));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		$json['event'] = array('id' => $model->id);
		echo CJSON::encode($json);

		$model->delete();
		$this->logAction();
		Yii::app()->end();
	}

	public function ordersList($id)
	{

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.schedule_id = ' . Yii::app()->db->quoteValue($id));

		$data = new CActiveDataProvider(new Order, array(
			'criteria' => $criteria,
		));


		Yii::app()->clientScript->scriptMap['bbq'] = false;
		Yii::app()->clientScript->packages['bbq'] = false;
		Yii::app()->clientScript->corePackages['bbq'] = false;
		Yii::app()->clientScript->packages['select2'] = false;
		Yii::app()->clientScript->packages['bootstrap.js'] = false;
		Yii::app()->clientScript->clearJquery();

		$this->renderPartial('ordersList', array('data' => $data));
	}

	public function loadModel($id)
	{
		$model = Schedule::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'schedule-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
} // End Welcome
