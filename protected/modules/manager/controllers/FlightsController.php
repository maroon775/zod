<?php

class FlightsController extends MainController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array(
					'create',
					'delete',
					'index',
					'update',
					'itemslist',
				),
				'roles'   => array('administrator', 'manager'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->actionUpdate();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id = null)
	{
		$model = $id === null ? new Flights() : $this->loadModel($id);
		$view = $id === null ? 'create' : 'update';

// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Flights']))
		{
			$model->attributes = $_POST['Flights'];
			foreach ($_POST['Flights'] as $k => $v)
			{
				if (in_array($k, array('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun')))
				{
					if (empty($v))
					{
						$model->{$k} = null;
					}
				}
			}

			if ($model->save())
				$this->logAction()->redirect(array('update', 'id' => $model->id));
		}

		$this->render($view, array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest)
		{
// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			$this->logAction();
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Flights('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Flights']))
			$model->attributes = $_GET['Flights'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Flights::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'flights-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionItemsList()
	{
		$data = Flights::model()->findAll();


		$arResult = array(0 => Yii::t('ManagerModule.main', 'Choise'));
		foreach ($data as $item)
		{
			$arResult[$item->id] = $item->flight;
		}

		echo CJSON::encode($arResult);
		Yii::app()->end();
	}

}
