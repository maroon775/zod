<?php

class ContractorsController extends MainController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('profile'),
				'users'   => array('@'),
			),
			array('allow',
				'actions' => array('view', 'create', 'update', 'profile', 'index', 'delete', 'updatePassword'),
				'roles'   => array('administrator', 'manager'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->actionUpdate();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id = null)
	{
		$view = 'update';
		if (!$id)
		{
			$view = 'create';
			$model = new User;
		} else
		{
			$model = $this->loadModel($id);
		}


// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);


		if (isset($_POST['User']))
		{
			$model->attributes = $_POST['User'];
			if ($model->save() && $id == null)
			{
				$this->logAction()->redirect(array('update', 'id' => $model->id, 'createSuccess' => 'true'));
			}
		}

		$this->render($view, array('model' => $model,));
	}


	public function actionUpdatePassword($id)
	{
		$model = $this->loadModel($id);
		if (Yii::app()->request->isPostRequest && Yii::app()->user->isAdmin)
		{
			if (isset($_POST['User']))
			{
				$model->password = User::encodePassword($_POST['User']['password']);
				if ($model->save(false))
					$this->redirect(array('update', 'id' => $model->id));
			}
		}
		$this->render('updatepassword', array('model' => $model,));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);

			$model->delete();

			$this->logAction();
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new User('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['User']))
			$model->attributes = $_GET['User'];

		$this->render('index', array(
			'model' => $model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = User::model()->withRole('contractor')->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'contractor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
