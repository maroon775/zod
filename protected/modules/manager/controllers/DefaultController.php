<?php

class DefaultController extends MainController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{

		if(Yii::app()->user->isAuth && Yii::app()->user->isManager)
		{
			$this->redirect(array('//manager'));
		}

		$this->layout = '/layouts/login';
		$model= new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(array('//manager/'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(array('/site/auth'));
	}

	public function actionIndex()
	{
		$this->render('index');
	}
}
