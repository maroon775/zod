<?

class OrderController extends MainController
{
	public function actionCreate($schedule_id = null)
	{
		$this->actionUpdate(null, $schedule_id);
	}

	public function actionUpdate($id = null, $schedule_id = null)
	{
		$model = $id === null ? new Order() : $this->loadModel($id);
		$action = $id === null ? 'create' : 'update';

		$this->performAjaxValidation($model);

		$model->status_id = OrderStatus::REQUESTED;
		$model->order_date = date('Y-m-d H:i:s');

		if (!empty($schedule_id))
			$model->schedule_id = $schedule_id;

		if (Yii::app()->user->isContractor)
			$model->contractor_id = Yii::app()->user->id;

		if (!empty($_POST['Order']))
		{
			$model->attributes = $_POST['Order'];
			if ($model->save() && Yii::app()->request->isAjaxRequest)
			{
				$this->logAction();
				$json['ok'] = 1;
				echo CJSON::encode($json);
				Yii::app()->end();
			}
		}


		Yii::app()->clientScript->registerCoreScript('yiiactiveform');

		if (Yii::app()->request->isAjaxRequest)
		{
			Yii::app()->clientScript->scriptMap['bbq'] = false;
			Yii::app()->clientScript->packages['bbq'] = false;
			Yii::app()->clientScript->corePackages['bbq'] = false;
			Yii::app()->clientScript->packages['select2'] = false;
			Yii::app()->clientScript->clearJquery();

			$this->renderPartial($action, array('model' => $model), false, true);
		} else
			$this->render($action, array('model' => $model));
	}

	public function loadModel($id)
	{
		$model = Order::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}


	public function actionQuickedit()
	{
		if (Yii::app()->request->getPost('pk'))
			new CHttpException(404, 'The requested page does not exist.');

		$model = $this->loadModel(Yii::app()->request->getPost('pk'));

		if (!empty($_POST))
		{
			$model->{$_POST['name']} = $_POST['value'];
			$model->save();
		}
		echo 1;
		Yii::app()->end();
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionIndex()
	{
		$model = new Order('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Order']))
			$model->attributes = $_GET['Order'];

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->render('view',array('model'=>$model));
	}
}

?>
