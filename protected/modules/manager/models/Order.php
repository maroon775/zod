<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property integer $contractor_id
 * @property string  $order_date
 * @property string  $fio
 * @property integer $age_id
 * @property string  $email
 * @property integer $phone
 * @property integer $passport
 * @property integer $status_id
 * @property integer $schedule_id
 * @property integer $invoice_id
 * @property string $Flight
 * @property string $FlightDate
 */
class Order extends CActiveRecord
{
	public $flight;
	public $flightDate;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contractor_id, order_date, fio, age_id, email, phone, passport, status_id, schedule_id', 'required'),
			array('contractor_id, age_id, passport, status_id, schedule_id, invoice_id', 'numerical', 'integerOnly' => true),
			array('fio', 'length', 'max' => 300),
			array('email, phone', 'length', 'max' => 50),
			array('email', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contractor_id, order_date, fio, age_id, email, phone, passport, status_id, schedule_id, invoice_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'age'      => array(self::BELONGS_TO, 'Age', 'age_id'),
			'schedule' => array(self::BELONGS_TO, 'Schedule', 'schedule_id'),
			'user'     => array(self::BELONGS_TO, 'User', 'contractor_id'),
//			'invoice'    => array(self::BELONGS_TO, 'Invoice','invoice_id'),
			'status'   => array(self::BELONGS_TO, 'OrderStatus', 'status_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'            => Yii::t('ManagerModule.main', 'ID'),
			'contractor_id' => Yii::t('ManagerModule.main', 'Contractor'),
			'order_date'    => Yii::t('ManagerModule.main', 'Order Date'),
			'fio'           => Yii::t('ManagerModule.main', 'Fio'),
			'age_id'        => Yii::t('ManagerModule.main', 'Age'),
			'email'         => Yii::t('ManagerModule.main', 'Email'),
			'phone'         => Yii::t('ManagerModule.main', 'Phone'),
			'passport'      => Yii::t('ManagerModule.main', 'Passport'),
			'status_id'     => Yii::t('ManagerModule.main', 'Status'),
			'status'        => Yii::t('ManagerModule.main', 'Status'),
			'schedule_id'   => Yii::t('ManagerModule.main', 'schedule'),
			'invoice_id'    => Yii::t('ManagerModule.main', 'Invoice'),
			'flight'        => Yii::t('ManagerModule.main', 'Flight'),
			'flightDate'    => Yii::t('ManagerModule.main', 'Flight date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('contractor_id', $this->contractor_id);
		$criteria->compare('order_date', $this->order_date, true);
		$criteria->compare('fio', $this->fio, true);
		$criteria->compare('age_id', $this->age_id);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone);
		$criteria->compare('passport', $this->passport);
		$criteria->compare('status_id', $this->status_id);
		$criteria->compare('schedule_id', $this->schedule_id);
		$criteria->compare('invoice_id', $this->invoice_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
