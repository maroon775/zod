<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $forgot
 * @property integer $phone_number
 * @property string $date_create
 * @property string $name
 * @property string $last_login
 * @property string $role
 * @property string $recovery_key
 * @property string $recovery_password
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, email, password, name, phone_number, role', 'required'),
			array('phone_number', 'numerical', 'integerOnly' => true),
			array('login, email', 'checkIfAvailable'),
			array('login', 'length', 'max' => 150),
			array('email', 'email'),
			array('email, name', 'length', 'max' => 300),
			array('password, forgot', 'length', 'max' => 100, 'min' => 6),
			array('recovery_key, recovery_password', 'length', 'max' => 50),
			array('role', 'length', 'max' => 50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, email, phone_number, date_create, name, role', 'safe', 'on' => 'search'),
		);
	}


	public function checkIfAvailable($attr)
	{
		$labels = $this->attributeLabels();
		$check  = User::model()->countByAttributes(array(
			                                           $attr => $this->$attr,
		                                           ), 't.id != :id', array(':id' => (int)$this->id));

		if ($check > 0)
			$this->addError($attr, Yii::t('main', '{attr} "{value}" уже занят другим пользователем.', array('{attr}' => $labels[$attr], '{value}' => $this->$attr)));
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'           => Yii::t('ManagerModule.main', 'ID'),
			'login'        => Yii::t('ManagerModule.main', 'Login'),
			'email'        => Yii::t('ManagerModule.main', 'Email'),
			'password'     => Yii::t('ManagerModule.main', 'Password'),
			'name'         => Yii::t('ManagerModule.main', 'Name'),
			'phone_number' => Yii::t('ManagerModule.main', 'Phone Number'),
			'role'         => Yii::t('ManagerModule.main', 'Role'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = $this->getDbCriteria();

		$criteria->compare('id', $this->id);
		$criteria->compare('login', $this->login, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('role', $this->role, true);

		$criteria->addCondition('id !=' . Yii::app()->user->id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getAllowedRoles($key = false)
	{
		$data = array(
			'user'          => Yii::t('ManagerModule.main', 'User'),
			'manager'       => Yii::t('ManagerModule.main', 'Manager'),
			'administrator' => Yii::t('ManagerModule.main', 'Administrator'),
			'employee'      => Yii::t('ManagerModule.main', 'Employee'),
			'contractor'    => Yii::t('ManagerModule.main', 'Contractor'),
		);

		return ($key ? $data[$key] : $data);
	}

	public function getFindAllowedRoles()
	{
		return self::getAllowedRoles();
	}

	public function getfriendlyRole()
	{
		return self::getAllowedRoles($this->role);
	}

	public static function encodePassword($string)
	{
		return md5($string);
	}

	public function beforeSave()
	{

		if ($this->isNewRecord)
		{
			if (!$this->role)
				$this->role = 'user';

			if (!$this->hasErrors())
				$this->password = User::encodePassword($this->password);
		}

		$this->date_create = date('Y-m-d H:i:s');

		return parent::beforeSave();
	}

	public function withRole($role)
	{
		if (!empty($role))
		{
			if (is_array($role))
				$this->getDbCriteria()->addInCondition('t.`role`', $role);
			else
				$this->getDbCriteria()->addCondition('t.`role` = ' . $this->getDbConnection()->quoteValue($role));
		}

		return $this;
	}
}
