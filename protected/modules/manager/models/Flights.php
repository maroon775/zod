<?php

/**
 * This is the model class for table "flights".
 *
 * The followings are the available columns in table 'flights':
 * @property string $id
 * @property string $flight
 * @property string $route
 * @property string $flighttype
 * @property string $flightclass
 * @property string $mon
 * @property string $tue
 * @property string $wed
 * @property string $thu
 * @property string $fri
 * @property string $sat
 * @property string $sun
 */
class Flights extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'flights';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flight, route, flighttype, flightclass', 'required'),
			array('flight', 'length', 'max' => 25),
			array('route', 'length', 'max' => 50),
			array('flighttype, flightclass', 'length', 'max' => 11),
			array('mon, tue, wed, thu, fri, sat, sun', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, flight, route, flighttype, flightclass, mon, tue, wed, thu, fri, sat, sun', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type'  => array(self::BELONGS_TO, 'Flighttypes', 'flighttype'),
			'class' => array(self::BELONGS_TO, 'Flightclasses', 'flightclass'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => Yii::t('ManagerModule.main','ID'),
			'flight'      => Yii::t('ManagerModule.main','Flight'),
			'route'       => Yii::t('ManagerModule.main','Route'),
			'flighttype'  => Yii::t('ManagerModule.main','Flight Type'),
			'flightclass' => Yii::t('ManagerModule.main','Flight Class'),
			'mon'         => Yii::t('ManagerModule.main','Mon'),
			'tue'         => Yii::t('ManagerModule.main','Tue'),
			'wed'         => Yii::t('ManagerModule.main','Wed'),
			'thu'         => Yii::t('ManagerModule.main','Thu'),
			'fri'         => Yii::t('ManagerModule.main','Fri'),
			'sat'         => Yii::t('ManagerModule.main','Sat'),
			'sun'         => Yii::t('ManagerModule.main','Sun'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('flight', $this->flight, true);
		$criteria->compare('route', $this->route, true);
		$criteria->compare('flighttype', $this->flighttype, true);
		$criteria->compare('flightclass', $this->flightclass, true);
		$criteria->compare('mon', $this->mon, true);
		$criteria->compare('tue', $this->tue, true);
		$criteria->compare('wed', $this->wed, true);
		$criteria->compare('thu', $this->thu, true);
		$criteria->compare('fri', $this->fri, true);
		$criteria->compare('sat', $this->sat, true);
		$criteria->compare('sun', $this->sun, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Flights the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
