<?php

/**
 * This is the model class for table "medialibrary".
 *
 * The followings are the available columns in table 'medialibrary':
 * @property integer $id
 * @property string $path
 * @property integer $size
 * @property string $extension
 */
class Medialibrary extends CActiveRecord
{
	const UPLOAD_PATH = '/uploads/medialibrary/';
	const THUMB_PATH = '/uploads/medialibrary/thumb/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'medialibrary';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path', 'required'),
			array('size', 'numerical', 'integerOnly' => true),
			//array('path', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1048576),
			array('extension', 'length', 'max' => 5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, path, size, extension', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => 'ID',
			'path'      => 'Path',
			'size'      => 'Size',
			'extension' => 'Extension',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('extension', $this->extension, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}


	public function beforeSave()
	{
		/**
	 * @var $file CUploadedFile
	 */
		if ($this->IsNewRecord && is_object($this->path))
		{
			$file = $this->path;
			$filename  =  uniqid() . "." . $file->extensionName;
			$path = Yii::getPathOfAlias('webroot') . self::UPLOAD_PATH . $filename ;
			$file->saveAs($path);

			$this->path = $filename;
			$this->size = $file->size;
			$this->extension = $file->extensionName;
		}
		return parent::beforeSave();
	}


	public function getUrl()
	{
		return Yii::app()->baseUrl . self::UPLOAD_PATH . $this->path;
	}


	public function resize($width, $height)
	{
		$thumbPath = self::THUMB_PATH . $width . "x" . $height . "/";
		$thumbSave = $thumbPath . $this->path;
		if (file_exists(Yii::getPathOfAlias('webroot') . $thumbSave))
			return Yii::app()->baseUrl . $thumbSave;

		if (!file_exists(Yii::getPathOfAlias('webroot') . $thumbPath))
			CFileHelper::createDirectory(Yii::getPathOfAlias('webroot') . $thumbPath, null, true);

		if (!file_exists(Yii::getPathOfAlias('webroot') . self::UPLOAD_PATH . $this->path))
			return false;

		$thumb = Yii::app()->image->create(Yii::getPathOfAlias('webroot') . self::UPLOAD_PATH . $this->path);
		$thumb->resize($width, $height);

		$thumb->save(Yii::getPathOfAlias('webroot') . $thumbSave);

		return Yii::app()->baseUrl.$thumbSave;
	}


	public function remove()
	{
		$filePath = Yii::getPathOfAlias('webroot') . self::UPLOAD_PATH . $this->path;
		if (file_exists($filePath))
			unlink($filePath);

		$this->delete();

		return true;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Medialibrary the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
}
