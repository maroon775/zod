<?php

/**
 * This is the model class for table "schedule".
 *
 * The followings are the available columns in table 'schedule':
 * @property string $id
 * @property string $flight_id
 * @property integer $fldate
 */
class Schedule extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'schedule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flight_id, datetime, fldate', 'required'),
			array('fldate', 'numerical', 'integerOnly' => true),
			array('fldate', 'uniqueFlightAndDate'),
			array('flight_id', 'length', 'max' => 11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('datetime', 'safe'),
			array('id, flight_id, datetime, fldate', 'safe', 'on' => 'search'),
		);
	}


	public function uniqueFlightAndDate($attribute, $params = array())
	{
		if (!$this->hasErrors())
		{
			$params['criteria'] = array(
				'condition' => 'flight_id=:flight_id',
				'params'    => array(':flight_id' => $this->flight_id),
			);
			$validator = CValidator::createValidator('unique', $this, $attribute, $params);

			return $validator->validate($this, array($attribute));
		}
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'flight' => array(self::BELONGS_TO, 'Flights', 'flight_id'),
			'orders' => array(self::HAS_MANY, 'Order', 'schedule_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => Yii::t('ManagerModule.main', 'ID'),
			'flight_id' => Yii::t('ManagerModule.main', 'Flight'),
			'fldate'    => Yii::t('ManagerModule.main', 'Flight date'),
			'datetime'  => Yii::t('ManagerModule.main', 'Datetime'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('flight_id', $this->flight_id, true);
		$criteria->compare('fldate', $this->fldate);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Schedule the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{

		return parent::beforeValidate();
	}

	public function getDatetime($format = 'Y-m-d H:i')
	{
		return date($format, $this->fldate);
	}
	public function setDatetime($value)
	{
		$this->fldate = strtotime($value);
		return $value;
	}

	public function ordersSearch()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('schedule_id', $this->id);

		return new CActiveDataProvider(new Order(), array(
			'criteria' => $criteria,
		));
	}
}
