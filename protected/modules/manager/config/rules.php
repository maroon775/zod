<?php
return [
	/* Manager */
	'<module:(administrator|manager|contractor|employee)>/'                               => 'manager/',
	'<module:(administrator|manager|contractor|employee)>/user'                           => 'manager/user/index',
	'<module:(administrator|manager|contractor|employee)>/user/<action:\w+>'              => 'manager/user/<action>',
	'<module:(administrator|manager|contractor|employee)>/calendar/<action:\w+>'          => 'manager/calendar/<action>',
	'<module:(administrator|manager|contractor|employee)>/schedule/<id:\d+>/<action:\w+>' => 'manager/schedule/<action>',
	'<module:(administrator|manager|contractor|employee)>/order/create/<schedule_id:\d+>' => 'manager/order/create',
	'<module:(administrator|manager|contractor|employee)>/<constroller:\w+>/<action:\w+>' => 'manager/<constroller>/<action>',
	'<module:(administrator|manager|contractor|employee)>/<action:\w+>'                   => 'manager/default/<action>',
];
