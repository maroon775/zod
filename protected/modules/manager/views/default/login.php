<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */
$this->pageTitle   = Yii::app()->name . ' - ' . Yii::t('ManagerModule.main', 'Authorization');
$this->breadcrumbs = array();
?>


<div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
	<h1 class="text-center"><?= Yii::t('ManagerModule.main', 'Authorization'); ?></h1>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'                     => 'login-form',
		'enableClientValidation' => true,
		'enableAjaxValidation'   => true,
		'clientOptions'          => array(
			'validateOnSubmit' => true,
		),
	)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'username'); ?>
		<?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
		<?php echo $form->error($model, 'username'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
		<?php echo $form->error($model, 'password'); ?>
	</div>

	<div class="form-group rememberMe">
		<?php echo $form->checkBox($model, 'rememberMe'); ?>
		<?php echo $form->label($model, 'rememberMe'); ?>
		<?php echo $form->error($model, 'rememberMe'); ?>
	</div>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton(Yii::t('ManagerModule.main', 'Log in'), array('class' => 'btn btn-block btn-primary')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
