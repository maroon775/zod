<?
/**
 * @var $this ScheduleController
 * @var $model Schedule
 * */
$this->widget('booster.widgets.TbDetailView', array(
	'id'         => 'ScheduleView',
	'data'       => $model,
	'attributes' => array(
		'id',
		'flight_id' => array(
			'name'  => 'flight_id',
			'value' => $model->flight->flight
		),
		'fldate'    => array(
			'name'  => 'fldate',
			'value' => $model->datetime
		),
	),
)); ?>
