<?
/**
 * @var $this ScheduleController
 * @var $form TbActiveForm
 * @var $model Schedule
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'                   => 'schedule-form',
	'enableAjaxValidation' => true,
	'htmlOptions'          => array('class' => 'text-left')
)); ?>
<?= $form->errorSummary($model); ?>

<div class="form-group">
	<?= $form->label($model, 'flight_id', array('class' => 'control-label')); ?>
	<div class="clearfix">
		<?$this->widget(
			'booster.widgets.TbSelect2',
			array(
				'id'          => CHtml::activeId($model, 'flight_id') . ($model->id ? '_' . $model->id : ''),
				'model'       => $model,
				'attribute'   => 'flight_id', // $model->name will be editable
				'value'       => '$data->flight->flight',
				'data'        => $this->modelArray('Flights', 'flight'),
				'htmlOptions' => array('style' => 'width:100%;', 'id' => CHtml::activeId($model, 'flight_id') . ($model->id ? '_' . $model->id : ''))
			)
		);?>
	</div>
</div>
<?= $form->dateTimePickerGroup($model, 'datetime', array()) ?>
<? if (!Yii::app()->request->isAjaxRequest): ?>
	<button type="submit" class="btn btn-warning btn-lg btn-block">
		<span class="glyphicon glyphicon-ok-sign"></span>
		<?= Yii::t('ManagerModule.main', 'Save') ?>
	</button>
<? endif ?>
<? $this->endWidget() ?>
