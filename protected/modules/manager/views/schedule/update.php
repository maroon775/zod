<?
/**
 * @var $this ScheduleController
 * @var $model Schedule
 * @var $form TbActiveForm
 * */
?>

<h4><?= Yii::t('ManagerModule.main', 'Edit schedule') ?> #<?=$model->id ?></h4>

<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<?
		if(
			Yii::app()->user->role == 'employee'
			|| count($model->orders) > 0
			|| $model->fldate < mktime(0,0,0,date('m'),date('d'),date('Y'))
		)
		{
			$this->renderPartial('view', array('model' => $model), false, false);
			echo '<style>#saveButtonModalForm{display: none;}</style>';
		}
		else
		{
			$this->renderPartial('_form', array('model' => $model));
			echo CHtml::button(
				Yii::t('ManagerModule.main', 'Delete'),
				array('class' => 'btn-danger btn-block btn-lg', 'id' => 'ScheduleDelete'));
				Yii::app()->clientScript->registerScript('ScheduleDelete', "
					$('#ScheduleDelete').one('click.scheduleDelete',function(){
						" . CHtml::ajax(array(
	                           'url'      => $this->createUrl('/manager/schedule/delete', array('id' => $model->id)),
	                           'dataType' => 'json',
	                           'success'  => 'js:function(data){
									$("#calendar").fullCalendar("removeEvents", data.event.id);
									$("#EventEditModal").modal("hide");
								}'
		                    )) . "
					})
				");
		}
		?>
	</div>
	<br/>
	<div class="col-xs-12 text-left">
		<?
		if(Yii::app()->user->isEmployee && $model->fldate >= mktime(0,0,0,date('m'),date('d'),date('Y')))
		{
			echo CHtml::button(
				Yii::t('ManagerModule.main', 'Add passanger'),
				array('class' => 'btn-primary btn-sm', 'id' => 'CreateOrder'));
			Yii::app()->clientScript->registerScript('ScheduleDelete', "
					$('#CreateOrder').unbind('click.createOrder').on('click.createOrder',function(){
						" . CHtml::ajax(array(
	                           'url'      => $this->createUrl('/manager/order/create/', array('schedule_id' => $model->id)),
	                           'dataType' => 'html',
	                           'success'  => 'js:function(data){
									$("#OrderFormWrapper").html(data);
									$("#OrderFormModal").modal("show");
								}'
		                    )) . "
					})
				");
		}
		?>
	</div>
</div>
<h4 class="pull-left"><?=Yii::t('ManagerModule.main','Passangers')?></h4>
<? $this->ordersList($model->id); ?>
