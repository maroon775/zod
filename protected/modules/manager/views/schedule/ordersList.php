<?
/**
 * @var $data Order
 * @var $this ScheduleController
 */
$columns = array(
	'id',
	'fio',
	'age_id',
	'phone',
	'email',
	array(
		'class'    => 'booster.widgets.TbEditableColumn',
		'name'     => 'status_id',
		'value'    => '$data->status->name',
		'sortable' => true,
		'editable' => array(
			'url'       => Yii::app()->createUrl('/manager/order/quickedit'),
			'type'      => 'select2',
			'placement' => 'top',
			'source'    => $this->orderStatusesArray()
		)
	),
	/*'buttons'=>array(
		'class'    => 'booster.widgets.TbButtonColumn',
		'template' => '{delete}',
		'buttons'  => array(
			'delete' => array(
				'url' => 'Yii::app()->createUrl("/manager/order/delete", array("id"=>$data->id))',
			),
		)
	),*/
);
if (!Yii::app()->user->isManager)
{
	$columns['status'] = array(
		'name'     => 'status_id',
		'value'    => '$data->status->name',
	);
	unset($columns['buttons']);
}

$this->widget(
	'booster.widgets.TbGridView',
	array(
		'id'=>'ScheduleOrdersTable',
		'type'          => 'striped bordered',
		'dataProvider'  => $data,
		'enableSorting' => false,
		'columns'       => $columns
	)
);
?>
