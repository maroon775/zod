<?
/**
 * @var $this ScheduleController
 * @var $model Schedule
 *
 * */
?>

<h4><?=Yii::t('ManagerModule.main','Create schedule') ?></h4>

<div class="col-sm-4 col-sm-offset-4">
<? $this->renderPartial('_form',array('model'=>$model)) ?>
</div>
