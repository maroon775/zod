<?
/**
 * @var $this OrderController
 * @var $model Order
 *
 * */
?>

<h4><?=Yii::t('ManagerModule.main','Create order') ?></h4>

<div class="col-sm-4 col-sm-offset-4">
	<? $this->renderPartial('view',array('model'=>$model->schedule)) ?>
	<? $this->renderPartial('_form',array('model'=>$model)) ?>
</div>
