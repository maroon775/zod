<?
/**
 * @var $form TbActiveForm
 * @var $this OrderController
 * @var $model Order
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

<?php echo $form->textFieldGroup($model, 'id', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5')))); ?>

<?php echo $form->textFieldGroup($model, 'fio', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 150)))); ?>

<?php echo $form->textFieldGroup($model, 'email', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 300)))); ?>

<?php echo $form->textFieldGroup($model, 'phone', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 300)))); ?>

<?php echo $form->dropDownListGroup($model, 'age_id', array('widgetOptions' => array('data' => $this->modelArray('Age', 'name'), 'htmlOptions' => array('class' => 'input-large')))); ?>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('ManagerModule.main', 'Search'),
	)); ?>
</div>

<?php $this->endWidget(); ?>
