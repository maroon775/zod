<?
/**
 * @var $this OrderController
 * @var $form TbActiveForm
 * @var $model Order
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'                   => 'order-form',
	'enableAjaxValidation' => true,
	'htmlOptions'          => array('class' => 'text-left')
)); ?>
<?= $form->errorSummary($model); ?>
<?=$form->hiddenField($model,'contractor_id');?>
<?=$form->hiddenField($model,'schedule_id');?>
<?=$form->hiddenField($model,'invoice_id');?>
<?=$form->hiddenField($model,'order_date');?>
<?=$form->textFieldGroup($model,'fio')?>

<div class="form-group">
	<?= $form->label($model, 'age_id', array('class' => 'control-label')); ?>
	<div class="clearfix">
		<?$this->widget(
			'booster.widgets.TbSelect2',
			array(
				'id'          => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''),
				'model'       => $model,
				'attribute'   => 'age_id', // $model->name will be editable
				'value'       => '$data->age->name',
				'data'        => $this->modelArray('Age', 'name'),
				'htmlOptions' => array('style' => 'width:100%;', 'id' => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''))
			)
		);?>
	</div>
</div>
<?=$form->textFieldGroup($model,'email')?>
<?=$form->textFieldGroup($model,'phone')?>
<?=$form->textFieldGroup($model,'passport')?>
<? if (!Yii::app()->request->isAjaxRequest): ?>
	<button type="submit" class="btn btn-warning btn-lg btn-block">
		<span class="glyphicon glyphicon-ok-sign"></span>
		<?= Yii::t('ManagerModule.main', 'Save') ?>
	</button>
<? endif ?>
<? $this->endWidget() ?>
