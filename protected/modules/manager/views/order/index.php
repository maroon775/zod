<div class="container-fluid">
	<?php
	/**
	 * @var $this OrderController
	 * @var $model Order
	 *
	 */

	$this->breadcrumbs = array(
		Yii::t('ManagerModule.main', 'Reports on orders') => array('/manager/order/index'),
		Yii::t('ManagerModule.main', 'Manage'),
	);
	Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
	});
	$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
	data: $(this).serialize()
	});
	return false;
	});
	");
	?>

	<h4><?= Yii::t('ManagerModule.main', 'Reports on orders') ?></h4>

	<p>
		<?= Yii::t('ManagerModule.main', 'You may optionally enter a comparison operator (
	<b>&lt;</b>
	,
	<b>&lt;=</b>
	,
	<b>&gt;</b>
	,
	<b>&gt;=</b>
	,
	<b>&lt;&gt;</b>
	or
	<b>=</b>
	) at the beginning of each of your search values to specify how the comparison should be done.')?>
	</p>

	<?php echo CHtml::link(Yii::t('ManagerModule.main', 'Advanced Search'), '#', array('class' => 'search-button btn')); ?>
	<div class="row search-form" style="display:none">
		<div class="col-sm-6">
			<?php $this->renderPartial('_search', array(
				'model' => $model,
			)); ?>
		</div>
	</div>
	<!-- search-form -->
	<?php $this->widget('booster.widgets.TbGridView', array(
		'id'           => 'user-grid',
		'dataProvider' => $model->search(),
		'filter'       => $model,
		'columns'      => array(
			array(
				'name'=>'id',
			),
			'order_date' => array(
				'name'   => 'order_date',
				'value'=>'date("Y-m-d H:i",strtotime($data->order_date))',
				'filter' => false,
			),
			array(
				'name'  => 'flight',
				'value' => '$data->schedule->flight->flight',
			),
			array(
				'header' => Yii::t('ManagerModule.main', 'Flight date'),
				'value'  => 'date("Y-m-d H:i",$data->schedule->fldate)'
			),
			array(
				'name'    => 'contractor_id',
				'visible' => Yii::app()->user->isEmployee,
				'value'   => '$data->user->name." (".$data->user->friendlyRole.")"'
			),
			'fio',
			'email',
			'phone',
			'age_id'     => array(
				'id'     => 'age_id',
				'name'   => 'age_id',
				'value'  => '$data->age->name',
				'filter' => $this->modelArray('Age', 'name')
			),
			array(
				'name'   => 'status',
				'value'  => '$data->status->name',
				'filter' => $this->modelArray('OrderStatus', 'name')
			),
			array(
				'class'           => 'booster.widgets.TbButtonColumn',
				'template'        => '{update}&nbsp;&nbsp;{view}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl("/manager/order/update",array("id"=>$data->primaryKey))',
//				'deleteButtonUrl' => 'Yii::app()->controller->createUrl("/manager/order/delete",array("id"=>$data->primaryKey))',
				'viewButtonUrl'   => 'Yii::app()->controller->createUrl("/manager/order/view",array("id"=>$data->primaryKey))',
			),
		),
	)); ?>

</div>
