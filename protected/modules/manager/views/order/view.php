<?
/**
 * @var $this OrderController
 * @var $model Order
 * */
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Order') => array('/manager/order/index'),
	'#'.$model->id,
);
$this->menu        = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Reports on orders'),
		'url'     => array('/manager/order/index')
	)
);
?>
<h4><?= Yii::t('ManagerModule.main', 'View order') ?> <?php echo $model->id; ?></h4>
<?
$this->widget('booster.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'order_date',
		'fio',
		'email',
		'phone',
		'passport',
		'age_id' => array(
			'name'  => 'age_id',
			'value' => $model->age->name
		),
	),
)); ?>
