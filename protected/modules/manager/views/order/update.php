<?
/**
 * @var $this OrderController
 * @var $model Order
 * @var $form TbActiveForm
 * */
?>

<h4><?= Yii::t('ManagerModule.main', 'Edit order') ?> <?= $model->id ?></h4>

<div class="col-sm-4 col-sm-offset-4">
<?
	$this->renderPartial('_form', array('model' => $model));
?>
	<br/>
</div>
<br/>
