<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Contractors') => array('index'),
	Yii::t('ManagerModule.main', 'Update'),
);

$this->menu = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Create contractor'),
		'url'     => array('/manager/contractors/create')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'View contractor'),
		'url'     => array('/manager/contractors/view', 'id' => $model->id)
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Manage contractors'),
		'url'     => array('/manager/contractors/index')
	),
);
?>
<? if (!empty($_REQUEST['createSuccess'])): ?>
	<script>
		$.notify('Контрагент <?=$model->name?>, успешно создан','success');
	</script>
<? endif ?>

<h4><?= Yii::t('ManagerModule.main', 'Update contractor') ?> <?= $model->name; ?></h4>

<?=$this->renderPartial('_form', array('model' => $model)); ?>
