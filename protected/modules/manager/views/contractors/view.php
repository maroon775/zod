<?php
/**
 * @var $model User
 * @var $this UserController
 * */
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Contractors') => array('index'),
	$model->name,
);

$this->menu = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Create contractor'),
		'url'     => array('/manager/contractors/create')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Update contractor'),
		'url'     => array('/manager/contractors/update', 'id' => $model->id)
	),
	array(
		'label'       => Yii::t('ManagerModule.main', 'Delete contractor'),
		'url'         => '#',
		'linkOptions' => array(
			'submit'  => array('delete', 'id' => $model->id),
			'confirm' => 'Are you sure you want to delete this item?'
		)
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Manage contractor'),
		'url'     => array('/manager/contractors/index')
	),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'View contractor') ?> <?php echo $model->name; ?></h4>

<?php $this->widget('booster.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'id',
		'login',
		'email',
		array(
			'name'=>'phone_number',
			'value'=>$model->phone_number?$model->phone_number:Yii::t('ManagerModule.main','empty')
		),
		'name',
		'role' => array('name' => 'role', 'value' => $model->getAllowedRoles($model->role)),
	),
)); ?>
