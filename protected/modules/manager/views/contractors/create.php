<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Contractors') => array('/manager/contractors/index'),
	Yii::t('ManagerModule.main', 'Create'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Manage contractors'), 'url' => array('/manager/contractors/index')),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'Create contractor') ?></h4>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
