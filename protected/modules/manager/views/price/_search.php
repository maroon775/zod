<?
/**
 * @var $this PriceController
 * @var $model Price
 * @var $form TbActiveForm
 */
?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

<?= $form->textFieldGroup($model, 'id'); ?>
<div class="form-group">
	<?= $form->label($model, 'age_id', array('class' => 'control-label')); ?>
	<div class="clearfix">
		<?$this->widget(
			'booster.widgets.TbSelect2',
			array(
				'id'          => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''),
				'model'       => $model,
				'attribute'   => 'age_id', // $model->name will be editable
				'value'       => '$data->age->name',
				'data'        => $this->modelArray('Age','name'),
				'htmlOptions' => array('class' => 'test', 'id' => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''))
			)
		);?>
	</div>
</div>
<?= $form->textFieldGroup($model, 'price'); ?>
<?= $form->dateTimePickerGroup($model, 'date_from'); ?>
<?= $form->dateTimePickerGroup($model, 'date_to'); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('ManagerModule.main', 'Search'),
	)); ?>
</div>

<?php $this->endWidget(); ?>
