<?php
$this->breadcrumbs=array(
	Yii::t('ManagerModule.main','Prices')=>array('/manager/price/index'),
	Yii::t('ManagerModule.main','Update'),
);

	$this->menu=array(
	array('label'=>Yii::t('ManagerModule.main','Create price'),'url'=>array('/manager/price/create')),
	array('label'=>Yii::t('ManagerModule.main','View price'),'url'=>array('/manager/price/view','id'=>$model->id)),
	array('label'=>Yii::t('ManagerModule.main','Manage price'),'url'=>array('/manager/price/index')),
	);
	?>

	<h4><?=Yii::t('ManagerModule.main','Update price') ?> <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
