<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Prices') => array('/manager/price/index'),
	Yii::t('ManagerModule.main', 'Manage'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Create price'), 'url' => array('/manager/price/create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('price-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h4><?= Yii::t('ManagerModule.main', 'Manage prices') ?></h4>

<p>
	<?= Yii::t('ManagerModule.main', 'You may optionally enter a comparison operator (
	<b>&lt;</b>
	,
	<b>&lt;=</b>
	,
	<b>&gt;</b>
	,
	<b>&gt;=</b>
	,
	<b>&lt;&gt;</b>
	or
	<b>=</b>
	) at the beginning of each of your search values to specify how the comparison should be done.') ?>
</p>

<?php echo CHtml::link(Yii::t('ManagerModule.main', 'Advanced Search'), '#', array('class' => 'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
		'model' => $model,
	)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView', array(
	'id'           => 'price-grid',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => array(
		'id',
		'age_id'=>array(
//			'class'=>'booster.widgets.TbSelect2',
			'name'=>'age_id',
			'value'=>'$data->age->name',
			'filter'=>$this->modelArray('Age','name')

		),
		'price',
		'date_from',
		'date_to',
		array(
			'class' => 'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
