<?
/**
 * @var $this PriceController
 * @var $model Price
 * @var $form TbActiveForm
 */
?>
<div class="col-xs-5">
	<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'                   => 'price-form',
		'enableAjaxValidation' => false,
	)); ?>

	<p class="help-block"><?=Yii::t('ManagerModule.main','Fields with <span class="required">*</span> are required.')?></p>

	<?=$form->errorSummary($model); ?>
	<div class="form-group">
		<?= $form->label($model, 'age_id', array('class' => 'control-label')); ?>
		<div class="clearfix">
			<?$this->widget(
				'booster.widgets.TbSelect2',
				array(
					'id'          => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''),
					'model'       => $model,
					'attribute'   => 'age_id', // $model->name will be editable
					'value'       => '$data->age->name',
					'data'        => $this->modelArray('Age','name'),
					'htmlOptions' => array('class' => 'test', 'id' => CHtml::activeId($model, 'age_id') . ($model->id ? '_' . $model->id : ''))
				)
			);?>
		</div>
	</div>
	<?=$form->textFieldGroup($model, 'price'); ?>
	<?=$form->dateTimePickerGroup($model, 'date_from'); ?>
	<?=$form->dateTimePickerGroup($model, 'date_to'); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'    => 'primary',
			'label'      => Yii::t('ManagerModule.main',$model->isNewRecord ? 'Create' : 'Save'),
		)); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
