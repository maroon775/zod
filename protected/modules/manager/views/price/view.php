<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Prices') => array('/manager/price/index'),
	$model->id,
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Create price'), 'url' => array('/manager/price/create')),
	array('label' => Yii::t('ManagerModule.main', 'Update price'), 'url' => array('/manager/price/update', 'id' => $model->id)),
	array('label' => Yii::t('ManagerModule.main', 'Delete price'), 'url' => '#', 'linkOptions' => array('submit' => array('/manager/price/delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => Yii::t('ManagerModule.main', 'Manage prices'), 'url' => array('/manager/price/index')),
);
?>

<h4><?=Yii::t('ManagerModule.main','View price') ?> #<?php echo $model->id; ?></h4>

<?php $this->widget('booster.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'id',
		'age_id',
		'price',
		'date_from',
		'date_to',
	),
)); ?>
