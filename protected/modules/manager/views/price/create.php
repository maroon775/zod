<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Prices') => array('/manager/price/index'),
	Yii::t('ManagerModule.main', 'Create'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Manage price'), 'url' => array('/manager/price/index')),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'Create price') ?></h4>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
