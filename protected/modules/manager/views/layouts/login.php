<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection"/>
	<![endif]-->
	<title><?CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<div class="b-manager-header">
			<h1 class="page-header text-center"><?=Yii::t('ManagerModule.main', 'Site Manager')?></h1>
			<br/>
			<div class="col-sm-4 col-sm-offset-4">
				<a class="btn btn-warning btn-block text-center" href="<?= Yii::app()->createUrl('/') ?>">
					<b class="glyphicon glyphicon-arrow-left"></b>&nbsp;
					<?= Yii::t('ManagerModule.main', 'Site') ?>
				</a>
			</div>
		</div>
		<?=$content?>
	</div>
</body>
</html>
