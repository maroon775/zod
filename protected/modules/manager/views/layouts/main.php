<?
/**
 * @var CalendarController
*/
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection"/>
	<![endif]-->
	<title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="preloader" style="display: none;">
	<img src="<?= Yii::app()->request->baseUrl ?>/static/img/loader.gif" class="loader" alt="" exif="false">
</div>
<div class="b-manager-header col-xs-12" style="padding-top: 60px">
	<?
	$this->widget(
		'booster.widgets.TbNavbar',
		array(
			'collapse' => true,
			'fixed'    => 'top',
			'fluid'    => false,
			'brand'    => '<i class="fa fa-signal success-color"></i> ЗОД - <b>Манас</b>',
			'items'    => array(
				array(
					'class'       => 'booster.widgets.TbMenu',
					'type'        => 'navbar',
					'encodeLabel' => false,
					'htmlOptions' => array('class' => 'navbar-left'),
					'items'       => array(
						array(
							'label'  => Yii::t('ManagerModule.main', 'Calendar'),
							'url'    => $this->createUrl('/manager/calendar/index'),
							'active' => Yii::app()->controller->id == 'calendar',
						),
						array(
							'label'   => Yii::t('ManagerModule.main', 'Flights'),
							'url'     => 'javascript:;',
							'active'  => Yii::app()->controller->id == 'flights',
							'visible' => Yii::app()->user->isManager,
							'items'   => array(
								array(
									'label'   => Yii::t('ManagerModule.main', 'Manage'),
									'url'     => array('/manager/flights/index'),
									'visible' => Yii::app()->user->isManager
								),
								array(
									'label'   => Yii::t('ManagerModule.main', 'Add'),
									'url'     => array('/manager/flights/create'),
									'visible' => Yii::app()->user->isManager
								),
							),
						),
						array(
							'label'  => Yii::t('ManagerModule.main', 'Users'),
							'url'    => 'javascript:;',
							'active' => Yii::app()->controller->id == 'user',
							'items'  => array(
								array(
									'label' => Yii::t('ManagerModule.main', 'Main profile'),
									'url'   => array('/manager/user/profile'),
								),
								array(
									'label'   => Yii::t('ManagerModule.main', 'Manage'),
									'url'     => array('/manager/user/index'),
									'visible' => Yii::app()->user->isAdmin
								),
								array(
									'label'   => Yii::t('ManagerModule.main', 'Add'),
									'url'     => array('/manager/user/create'),
									'visible' => Yii::app()->user->isAdmin
								),
							),
						),
						array(
							'label'   => Yii::t('ManagerModule.main', 'Contractors'),
							'url'     => 'javascript:;',
							'active'  => Yii::app()->controller->id == 'contractors',
							'visible' => Yii::app()->user->isManager,
							'items'   => array(
								array(
									'label'   => Yii::t('ManagerModule.main', 'Manage'),
									'url'     => array('/manager/contractors/index'),
									'visible' => Yii::app()->user->isManager
								),
								array(
									'label'   => Yii::t('ManagerModule.main', 'Add'),
									'url'     => array('/manager/contractors/create'),
									'visible' => Yii::app()->user->isManager
								),
							),
						),
						array(
							'label'   => Yii::t('ManagerModule.main', 'Reports on orders'),
							'url'     => array('/manager/order/index'),
							'active'  => Yii::app()->controller->id == 'order',
							'visible' => Yii::app()->user->isEmployee || Yii::app()->user->isContactor
						)
					),
				),
				array(
						'class'       => 'booster.widgets.TbMenu',
						'type'        => 'navbar',
						'encodeLabel' => false,
						'htmlOptions' => array('class' => 'navbar-right'),
						'items'       => array(
							array(
								'label'       => Yii::t('ManagerModule.main', 'Logout') . '&nbsp;<b class="glyphicon glyphicon-log-out"></b>',
								'url'         => array('/manager/default/logout'),
								'itemOptions' => array('class' => 'pull-right'),
							),
							array(
								'label'       => Yii::t('ManagerModule.main', 'Site') . '&nbsp;<b class="glyphicon glyphicon-globe"></b>',
								'url'         => array('/'),
								'itemOptions' => array('class' => 'pull-right'),
								'linkOptions' => array('target' => '_blank')
							),
							array(
								'label'       => Yii::t('ManagerModule.main', 'Site Manager') . '&nbsp;<b class="glyphicon glyphicon-home"></b>',
								'url'         => array('/manager/'),
								'itemOptions' => array('class' => 'pull-right'),
								'linkOptions' => array('target' => '_blank')
							)
						)
					)
			),
		)
	);
	?>
	<div>
		<div>
			<?php if (!empty($this->breadcrumbs)): ?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'homeLink'             => false,
					'activeLinkTemplate'   => '<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate' => '<li class="active">{label}</li>',
					'separator'            => false,
					'links'                => array_merge(array(Yii::t('ManagerModule.main', 'Home') => $this->createUrl('/manager/')), $this->breadcrumbs),
					'tagName'              => 'ul',
					'htmlOptions'          => array('class' => 'breadcrumb'),
				)); ?><!-- breadcrumbs -->
			<?php endif ?>
		</div>
	</div>


	<div class="row">
		<? if (!empty($this->menu)): ?>
		<div class="col-sm-3">
			<?
			$this->widget(
				'booster.widgets.TbMenu',
				array(
					'encodeLabel' => false,
					'type'        => 'list',
					'items'       => array_merge(
						array(array(
							'label'       => CHtml::tag(
								'h4',
								array(),
								Yii::t('ManagerModule.main', 'Available operations')
							),
							'itemOptions' => array('class' => 'nav-header')
						)),
						$this->menu
					)
				)
			);
			?>
		</div>

		<div class="col-sm-9">
			<? else: ?>
			<div>
				<? endif; ?>
				<?= $content ?>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</body>
</html>
