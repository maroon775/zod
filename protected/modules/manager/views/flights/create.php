<?php
$this->breadcrumbs=array(
	Yii::t('ManagerModule.main','Flights')=>array('/manager/flights/index'),
	Yii::t('ManagerModule.main','Create'),
);

$this->menu=array(
array('label'=>Yii::t('ManagerModule.main','Manage flights'),'url'=>array('/manager/flights/index')),
);
?>

<h4><?=Yii::t('ManagerModule.main','Create flight') ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
