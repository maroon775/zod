<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main','Flights') => array('/manager/flights/index'),
	Yii::t('ManagerModule.main','Manage'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main','Create flight'), 'url' => array('/manager/flights/create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function ()
	{
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function ()
	{
		$.fn.yiiGridView.update('catalog-modification-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h4><?=Yii::t('ManagerModule.main','Manage flights')?></h4>

<p>
	<?=Yii::t('ManagerModule.main','You may optionally enter a comparison operator (
	<b>&lt;</b>
	,
	<b>&lt;=</b>
	,
	<b>&gt;</b>
	,
	<b>&gt;=</b>
	,
	<b>&lt;&gt;</b>
	or
	<b>=</b>
	) at the beginning of each of your search values to specify how the comparison should be done.') ?>
</p>

<?php echo CHtml::link(Yii::t('ManagerModule.main','Advanced Search'), '#', array('class' => 'search-button btn btn-default')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
		'model' => $model,
	)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView', array(
	'id'           => 'catalog-modification-grid',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => array(
		'id',
		'flight',
		'route',
		'flighttype'=>array(
			'name'=>'flighttype',
			'value'=>'$data->type->name'
		),
		'flightclass'=>array(
			'name'=>'flightclass',
			'value'=>'$data->class->name'
		),
		array(
			'class' => 'booster.widgets.TbButtonColumn',
			'template'=>'{update}&nbsp;&nbsp;{delete}'
		),
	),
)); ?>
