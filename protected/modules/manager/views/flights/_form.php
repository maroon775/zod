<? /**
 * @var $this FlightsController
 * @var $model Flights
 * @var $form TbActiveForm
 */
?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'                   => 'flights-form',
	'enableAjaxValidation' => true,
)); ?>

<p class="help-block"><? Yii::t('ManagerModule.main', 'Fields with <span class="required">*</span> are required.') ?></p>

<?= $form->errorSummary($model); ?>
<div class="row col-xs-6">
	<?= $form->textFieldGroup($model, 'flight', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 300)))); ?>
	<?= $form->textFieldGroup($model, 'route', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 300)))); ?>

</div>
<div class="row">
	<div class="form-group col-xs-6">
		<?= $form->label($model, 'flighttype', array('class' => 'control-label')); ?>
		<div class="clearfix">
			<?$this->widget(
				'booster.widgets.TbSelect2',
				array(
					'id'          => CHtml::activeId($model, 'flighttype') . ($model->id ? '_' . $model->id : ''),
					'model'       => $model,
					'attribute'   => 'flighttype', // $model->name will be editable
					'value'       => '$data->type->name',
					'data'        => $this->modelArray('Flighttypes', 'name'),
					'htmlOptions' => array('class' => 'test', 'id' => CHtml::activeId($model, 'flighttype') . ($model->id ? '_' . $model->id : ''))
				)
			);?>
		</div>
	</div>
	<div class="form-group col-xs-6">
		<?= $form->label($model, 'flightclass', array('class' => 'control-label')); ?>
		<div class="clearfix">
			<?$this->widget(
				'booster.widgets.TbSelect2',
				array(
					'id'          => CHtml::activeId($model, 'flightclass') . ($model->id ? '_' . $model->id : ''),
					'model'       => $model,
					'attribute'   => 'flightclass', // $model->name will be editable
					'value'       => '$data->class->name',
					'data'        => $this->modelArray('Flightclasses', 'name'),
					'htmlOptions' => array('class' => 'test', 'id' => CHtml::activeId($model, 'flightclass') . ($model->id ? '_' . $model->id : ''))
				)
			);?>
		</div>
	</div>
</div>
<div class="row">
	<?= $form->timePickerGroup($model, 'mon', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'tue', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'wed', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'thu', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'fri', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'sat', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
<?= $form->timePickerGroup($model, 'sun', array(
		'groupOptions' => array('class' => 'col-md-4'),
	'widgetOptions' => array(
		'noAppend'           => true,
		'options'            => array(
			'defaultTime'  => false,
			'showMeridian' => false,
		)
	)
)) ?>
</div>
<div class="form-actions">
	<?php
	$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('ManagerModule.main', $model->isNewRecord ? 'Create' : 'Save'),
	)); ?>

</div>

<?php $this->endWidget(); ?>
