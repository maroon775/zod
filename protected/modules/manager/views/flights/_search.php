<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?=$form->textFieldGroup(
		$model,
		'id',
		array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))
	); ?>

	<?=$form->textFieldGroup(
		$model,
		'flight',
		array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>300)))
	); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'    => 'primary',
			'label'      => Yii::t('ManagerModule.main', 'Search'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
