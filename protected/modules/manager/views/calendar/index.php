<?php
/**
 * @var $this DefaultController
 */
?>
<div class="container content">
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div id='calendar'></div>
		</div>
		<div class="col-md-3 col-sm-3">
			<button class="btn btn-primary btn-block m-t" id="fillschedule">
				<i class="fa fa-arrow-down"></i>
				Заполнить календарь
			</button>
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Ближайшие рейсы</h5>
				</div>
				<div class="ibox-content">
					<div>
						<div class="feed-activity-list">
							<? foreach ($orders as $order): ?>
								<div class="feed-element">
									<div class="media-body ">
										<div>
											Заказ на рейс
											<b data-toggle="tooltip" data-title="<?= $order->schedule->getDatetime('d.m.Y H:i') ?>">
												<?= $order->schedule->flight->flight ?>
											</b>
											на имя "<?= $order->fio ?>"
										</div>
										<small class="text-muted">
											<?= $order->order_date ?>
										</small>

									</div>
								</div>
							<? endforeach ?>
						</div>
					</div>

				</div>
			</div>
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Последние действия</h5>
				</div>
				<div class="ibox-content">

					<div>
						<div class="feed-activity-list" id="act_list">

						</div>

						<button class="btn btn-primary btn-block m-t" name="act_button" data-target="#portfolioModal1" data-toggle="modal">
							<i class="fa fa-arrow-down"></i>
							Показать все
						</button>

					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- /row -->
</div>


<div class="portfolio-modal modal fade" id="EventEditModal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="modal-body" id="EventEditWrapper">
					</div>
					<div class="alert_placeholder"></div>
					<div class="modal-footer col-sm-4 col-sm-offset-4">
						<?php $this->widget(
							'booster.widgets.TbButton',
							array(
								'id'          => 'saveButtonModalForm',
								'buttonType'  => 'ajaxSubmit',
								'context'     => 'warning',
								'encodeLabel' => false,
								'label'       => '<span class="glyphicon glyphicon-ok-sign"></span> ' . Yii::t('ManagerModule.main', 'Save'),
								'url'         => 'js:$("#EventEditWrapper form").attr("action")',
								'ajaxOptions' => array(
									'dataType' => 'json',
									'data'     => 'js:$("#EventEditWrapper form").serialize()',
									'success'  => 'js:function(data){
										$("#EventEditModal").modal("hide");
										if(data.hasEventAdd)
										{
											$("#calendar").fullCalendar("addEventSource", data.event);
										}
										if(data.hasEventUpdate)
										{
											event = EventManager.getEventsSource(data.event.id);
											event.start = data.event.start;
											event.end = data.event.end;
											event.title = data.event.title;
											$("#calendar").fullCalendar("updateEvent", event);
										}
										if(data.notifyMessage)
										{
											$.notify(data.notifyMessage,{
												className:"success",
												globalPosition: "bottom left",
												showAnimation: "fadeIn",
												hideAnimation: "fadeOut"
											});
										}
									}'
								),
								'htmlOptions' => array(
									'class' => 'btn-block btn-lg'
								)
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<div class="portfolio-modal modal fade" id="OrderFormModal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-content">
		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="modal-body" id="OrderFormWrapper">
					</div>
					<div class="modal-footer col-sm-4 col-sm-offset-4">
						<?php $this->widget(
							'booster.widgets.TbButton',
							array(
								'id'          => 'orderSaveButtonModalForm',
								'buttonType'  => 'ajaxSubmit',
								'context'     => 'warning',
								'encodeLabel' => false,
								'label'       => '<span class="glyphicon glyphicon-ok-sign"></span> ' . Yii::t('ManagerModule.main', 'Save'),
								'url'         => 'js:$("#OrderFormWrapper form").attr("action")',
								'ajaxOptions' => array(
									'dataType' => 'json',
									'data'     => 'js:$("#OrderFormWrapper form").serialize()',
									'success'  => 'js:function(data){
										$("#OrderFormModal").modal("hide");
										if(data.notifyMessage)
										{
											$.notify(data.notifyMessage,{
												className:"success",
												globalPosition: "bottom left",
												showAnimation: "fadeIn",
												hideAnimation: "fadeOut"
											});
										}
									}'
								),
								'htmlOptions' => array(
									'class' => 'btn-block btn-lg'
								)
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
