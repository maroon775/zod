<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Users') => array('/manager/user/index'),
	Yii::t('ManagerModule.main', 'Create'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Manage Users'), 'url' => array('/manager/user/index')),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'Create User') ?></h4>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
