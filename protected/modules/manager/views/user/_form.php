<?
/**
 * @var $form TbActiveForm
 * @var $model User
 * @var $this UserController
 * */

?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'                   => 'user-form',
	'enableAjaxValidation' => true,

	'htmlOptions'          => array(
		'class' => 'col-xs-6',
		'action'=>$model->isNewRecord?$this->createUrl('create'):$this->createUrl('update',array('id'=>$model->id)),
	)
)); ?>

<p class="help-block"><?= Yii::t('ManagerModule.main', 'Fields with <span class="required">*</span> are required.') ?></p>

<?= $form->errorSummary($model); ?>
<?= $form->textFieldGroup(
	$model,
	'login',
	array(
		'widgetOptions' => array(
			'htmlOptions' => array(
				'class' => 'span5', 'maxlength' => 150)
		)
	)
); ?>
<?= $form->textFieldGroup(
	$model,
	'email',
	array(
		'widgetOptions' => array(
			'htmlOptions' => array(
				'class' => 'span5', 'maxlength' => 300)
		)
	)
); ?>
<?= $form->textFieldGroup(
	$model,
	'phone_number',
	array(
		'widgetOptions' => array(
			'htmlOptions' => array(
				'class' => 'span5', 'maxlength' => 300)
		)
	)
); ?>
<? if ($model->isNewRecord): ?>
	<?= $form->textFieldGroup(
		$model,
		'password',
		array(
			'widgetOptions' => array(
				'htmlOptions' => array(
					'class' => 'span5', 'maxlength' => 40)
			)
		)
	); ?>
<? endif ?>
<?= $form->textFieldGroup(
	$model,
	'name',
	array(
		'widgetOptions' => array(
			'htmlOptions' => array(
				'class' => 'span5', 'maxlength' => 300)
		)
	)
); ?>
<? if (Yii::app()->user->isAdmin): ?>
	<?= $form->dropDownListGroup(
		$model,
		'role',
		array(
			'widgetOptions' => array(
				'data'        => $model->getFindAllowedRoles(),
				'htmlOptions' => array('class' => 'input-large')
			)
		)
	); ?>
<? else: ?>
	<?= $form->dropDownListGroup(
		$model,
		'role',
		array(
			'widgetOptions' => array(
				'data'        => $model->getFindAllowedRoles(),
				'htmlOptions' => array(
					'class'    => 'input-large',
					'readonly' => true,
					'disabled' => true
				)
			)
		)
	); ?>

<? endif ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('ManagerModule.main', $model->isNewRecord ? 'Create' : 'Save'),
	)); ?>
</div>

<?php $this->endWidget(); ?>
