<?php
/**
 * @var $this UserController
 * @var $model User
 *
*/

$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Users') => array('/manager/user/index'),
	Yii::t('ManagerModule.main', 'Manage'),
);

$this->menu = array(
	array('label' => Yii::t('ManagerModule.main', 'Create User'), 'url' => array('/manager/user/create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('user-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h4><?= Yii::t('ManagerModule.main', 'Manage Users') ?></h4>

<p>
	<?= Yii::t('ManagerModule.main', 'You may optionally enter a comparison operator (
	<b>&lt;</b>
	,
	<b>&lt;=</b>
	,
	<b>&gt;</b>
	,
	<b>&gt;=</b>
	,
	<b>&lt;&gt;</b>
	or
	<b>=</b>
	) at the beginning of each of your search values to specify how the comparison should be done.') ?>
</p>

<?php echo CHtml::link(Yii::t('ManagerModule.main', 'Advanced Search'), '#', array('class' => 'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
		'model' => $model,
	)); ?>
</div><!-- search-form -->
<?php $this->widget('booster.widgets.TbGridView', array(
	'id'           => 'user-grid',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => array(
		'id',
		'name',
		'email',
		'login',
		'role' => array(
			'id'     => 'role',
			'name'   => 'role',
			'value'  => '$data->role',
			'filter' => $model->getFindAllowedRoles()
		),
		array(
			'class'    => 'booster.widgets.TbButtonColumn',
			'template' => '{update}&nbsp;&nbsp;{delete}'.(Yii::app()->user->isAdmin? '&nbsp;&nbsp;{passw}' : ''),
			'updateButtonUrl' => 'Yii::app()->controller->createUrl("/manager/user/update",array("id"=>$data->primaryKey))',
			'deleteButtonUrl' => 'Yii::app()->controller->createUrl("/manager/user/delete",array("id"=>$data->primaryKey))',
			'viewButtonUrl'   => 'Yii::app()->controller->createUrl("/manager/user/view",array("id"=>$data->primaryKey))',
			'buttons'=>array
			(
				'passw' => array
				(
					'label'=>'<i class="glyphicon glyphicon-play"></i>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("/manager/user/updatePassword", array("id"=>$data->id))',
					'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('ManagerModule.main', 'Password Recovery')),
				),
			),
		),
	),
)); ?>
