<?php
/**
 * @var $model User
 * @var $this UserController
 * */
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Users') => array('index'),
	$model->name,
);

$this->menu = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Create User'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/create')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Update User'),
		'visible' => Yii::app()->user->isAdmin || Yii::app()->user->id == $model->id,
		'url'     => array('/manager/user/update', 'id' => $model->id)
	),
	array(
		'label'       => Yii::t('ManagerModule.main', 'Delete User'),
		'visible'     => Yii::app()->user->isAdmin,
		'url'         => '#',
		'linkOptions' => array(
			'submit'  => array('/manager/user/delete', 'id' => $model->id),
			'confirm' => 'Are you sure you want to delete this item?'
		)
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Manage Users'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/index')
	),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'View User') ?> <?php echo $model->name; ?></h4>

<?php $this->widget('booster.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'id',
		'login',
		'email',
		'name',
		'role' => array('name' => 'role', 'value' => $model->getAllowedRoles($model->role)),
	),
)); ?>
