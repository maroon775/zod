<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Users') => array('/manager/user/index'),
	Yii::t('ManagerModule.main', 'Update'),
);

$this->menu = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Create User'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/create')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'View User'),
		'visible' => Yii::app()->user->isAdmin || $model->id == Yii::app()->user->id,
		'url'     => Yii::app()->user->isAdmin ? array('/manager/user/view', 'id' => $model->id) : array('profile')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Manage Users'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/index')
	),
);
?>
<? if (!empty($_REQUEST['createSuccess'])): ?>
	<script>
		$.notify('Пользователь <?=$model->name?>, успешно создан','success');
	</script>
<? endif ?>

<h4><?= Yii::t('ManagerModule.main', 'Update User') ?> <?= $model->name; ?></h4>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
