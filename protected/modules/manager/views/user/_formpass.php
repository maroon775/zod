<?
/**
 * @var $form TbActiveForm
 * */

?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'                   => 'user-form',
	'enableAjaxValidation' => false,

	'htmlOptions'          => array(
		'class' => 'col-xs-6',
		'action'=>$this->createUrl('updatePassword',array('id'=>$model->id)),
	)
)); ?>

<p class="help-block"><?= Yii::t('ManagerModule.main', 'Fields with <span class="required">*</span> are required.') ?>
</p>

<?= $form->errorSummary($model); ?>
	<div class="form-group"><label class="control-label" for="User_password">Пароль</label><input class="span5 form-control" maxlength="40" placeholder="Пароль" name="User[password]" id="User_password" type="text" value></div>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('ManagerModule.main', 'Save'),
	)); ?>
</div>

<?php $this->endWidget(); ?>
