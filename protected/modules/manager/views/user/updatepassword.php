<?php
$this->breadcrumbs = array(
	Yii::t('ManagerModule.main', 'Users') => array('/manager/user/index'),
	Yii::t('ManagerModule.main', 'Update'),
);

$this->menu = array(
	array(
		'label'   => Yii::t('ManagerModule.main', 'Create User'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/create')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'View User'),
		'visible' => Yii::app()->user->isAdmin || $model->id == Yii::app()->user->id,
		'url'     => Yii::app()->user->isAdmin ? array('/manager/user/view', 'id' => $model->id) : array('profile')
	),
	array(
		'label'   => Yii::t('ManagerModule.main', 'Manage Users'),
		'visible' => Yii::app()->user->isAdmin,
		'url'     => array('/manager/user/index')
	),
);
?>

<h4><?= Yii::t('ManagerModule.main', 'Update Password') ?> <?= $model->login . ' (name: '. $model->name .')'; ?></h4>

<?php echo $this->renderPartial('_formpass', array('model' => $model)); ?>
