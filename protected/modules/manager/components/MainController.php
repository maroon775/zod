<?php

class MainController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '/layouts/main';


	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	public $jqueryAccept = false;


	public function logAction()
	{
		$action = Yii::app()->controller->action;
		$actionDescription = array(
			'update'         => Yii::t('ManagerModule.main', 'action_update'),
			'delete'         => Yii::t('ManagerModule.main', 'action_delete'),
			'create'         => Yii::t('ManagerModule.main', 'action_create_new'),
			'updatepassword' => Yii::t('ManagerModule.main', 'action_create_new')
		);

		if (!empty($actionDescription[$action->id]))
		{
			$model = new Actions;
			$model->user_id = Yii::app()->user->id;
			$model->actdate = time();
			$model->action = $actionDescription[$action->id].' '.Yii::t('ManagerModule.main', 'action_object_' . $action->controller->id);
			$model->save();
		}

		return $this;
	}


	public function AgesArray()
	{
		$data = Age::model()->cache(Yii::app()->params['cacheTimeLong'])->findAll();


		$arResult = array();
		foreach ($data as $item)
		{
			$arResult[$item->id] = $item->name;
		}

		return $arResult;
	}

	public function orderStatusesArray()
	{
		$data = OrderStatus::model()->cache(Yii::app()->params['cacheTimeLong'])->findAll();


		$arResult = array();
		foreach ($data as $item)
		{
			$arResult[$item->id] = $item->name;
		}

		return $arResult;
	}

	public function modelArray($modelClass, $attribute)
	{
		$data = $modelClass::model()->cache(Yii::app()->params['cacheTimeLong'])->findAll();

		$arResult = array();
		foreach ($data as $item)
		{
			$arResult[$item->primaryKey] = $item->{$attribute};
		}

		return $arResult;
	}

	public function createUrl($route, $params = array(), $ampersand = '&')
	{
		if (Yii::app()->user->isAuth && $route != '/')
		{
			$params['module'] = Yii::app()->user->role;
		}

		return parent::createUrl($route, $params, $ampersand);
	}
}
