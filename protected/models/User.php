<?php

/**
 * This is the model class for table "s1_user".
 *
 * The followings are the available columns in table 's1_user':
 * @property integer $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $forgot
 * @property string $name
 * @property string $role
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, email, password, name', 'required', 'on' => 'register'),
			array('login, email', 'checkIfAvailable'),
			array('login', 'length', 'max' => 150),
			array('email', 'email'),
			array('email, name', 'length', 'max' => 300),
			array('password, forgot', 'length', 'max' => 100, 'min' => '6'),
			array('role', 'length', 'max' => 20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, email, password, forgot, name, role', 'safe', 'on' => 'search'),
		);
	}


	public function checkIfAvailable($attr)
	{
		$labels = $this->attributeLabels();
		$check = User::model()->countByAttributes(array(
			$attr => $this->$attr,
		), 't.id != :id', array(':id' => (int)$this->id));

		if ($check > 0)
			$this->addError($attr, Yii::t('main', '{attr} уже занят другим пользователем.', array('{attr}' => $labels[$attr])));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => 'ID',
			'login'     => Yii::t('main', 'Login'),
			'email'     => Yii::t('main', 'Email'),
			'password'  => Yii::t('main', 'Password'),
			'forgot'    => 'Forgot',
			'name'      => Yii::t('main', 'Name'),
			'role'      => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('login', $this->login, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('forgot', $this->forgot, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('role', $this->role, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getFullname()
	{
		return $this->name . ' ' . $this->last_name;
	}


	public function beforeSave()
	{

		if ($this->isNewRecord)
		{
			if (!$this->role)
				$this->role = 'user';

			if (!$this->hasErrors())
				$this->password = User::encodePassword($this->password);
		}

		return parent::beforeSave();
	}


	public static function encodePassword($string)
	{
		return md5($string);
	}


	public function isAdmin()
	{
		if ($this->role == "administrator") return true;

		return false;
	}


	public static function activeNewPassword($key)
	{
		$user = User::model()->findByAttributes(array('recovery_key' => $key));

		if (!$user)
			return false;

		$user->password = self::encodePassword($user->recovery_password);
		$user->recovery_key = '';
		$user->recovery_password = '';
		$user->save(false);

		return true;
	}

}
