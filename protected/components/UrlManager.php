<?

class UrlManager extends CUrlManager
{
	public function createUrl($route, $params = array(), $ampersand = '&')
	{
		if (
			Yii::app()->controller->module->id == 'manager'
		    && Yii::app()->user->isAuth
			&& !empty($route)
		)
		{
			$params['module'] = Yii::app()->user->role;
		}

		return parent::createUrl($route, $params, $ampersand);
	}
}
