<?

class EClientScript extends CClientScript
{
	public function clearJquery()
	{
		$this->scriptMap['jquery'] = false;
		$this->coreScripts['jquery'] = false;
		$this->packages['jquery'] = false;
		$this->corePackages['jquery'] = false;
	}
}

?>
