<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	private $_id;

	public $username;
	/**
	 * @var string password
	 */
	public $password;


	public function __construct($username = null, $password = false)
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function authenticate()
	{
		$user = User::model()->find('login = :login', array(':login' => $this->username));
		if ($user === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		elseif ($user->password !== User::encodePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id = $user->id;
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}

	public function authenticateID($id)
	{
		$user = User::model()->find('id = :id', array(':id' => $id));
		if ($user === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else
		{
			$this->_id = $user->id;
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}


	public function getId()
	{
		return $this->_id;
	}
}
