<?php

class BaseUser extends CWebUser
{

	/**
	 * @var int
	 */
	public $rememberTime = 2622600;

	/**
	 * @var User model
	 */
	private $_model;

	/**
	 * @return string user email
	 */
	public function getEmail()
	{
		$this->_loadModel();

		return $this->_model->email;
	}

	/**
	 * @return string username
	 */
	public function getUsername()
	{
		$this->_loadModel();

		return $this->_model->login;
	}

	/**
	 * Load user model
	 */
	private function _loadModel()
	{
		if (!$this->_model)
			$this->_model = User::model()->findByPk($this->id);
	}

	public function getModel()
	{
		$this->_loadModel();

		return $this->_model;
	}


	public function getRole()
	{
		$this->_loadModel();

		return $this->_model->role;
	}

	public function getIsManager()
	{
		if (Yii::app()->user->isAdmin)
			return true;

		return (isset(Yii::app()->user->role) && in_array( Yii::app()->user->role,array('manager')));
	}

	public function getIsContractor()
	{
		if (Yii::app()->user->isAdmin)
			return true;

		return (isset(Yii::app()->user->role) && Yii::app()->user->role == 'contractor');
	}

	public function getIsEmployee()
	{
		if (Yii::app()->user->isAdmin)
			return true;

		return (isset(Yii::app()->user->role) && in_array( Yii::app()->user->role,array('manager','employee')));
	}

	public function getIsAdmin()
	{
		return (isset(Yii::app()->user->role) && Yii::app()->user->role === 'administrator');
	}

	public function getIsAuth()
	{
		if ($user = $this->loadUser(Yii::app()->user->id))
			return true;

		return false;
	}

	protected function loadUser($id = null)
	{
		if ($id === null)
			return false;
		if ($this->_model === null)
		{
			if ($id !== null)
			{
				$this->_model = User::model()->findByPk($id);
			}
		}

		return $this->_model;
	}
}
