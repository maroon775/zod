<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array(
				'class' => 'CViewAction',
			),
		);
	}


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?sensor=false');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/modernizr-latest.js');
//		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/jquery-1.11.0.min.js');
//		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/bootstrap-3.1.1/js/bootstrap.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/fitvids/jquery.fitvids.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/select/bootstrap-select.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/gmap3/gmap.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/scrollTo/jquery.scrollTo.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/nav/jquery.nav.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/placeholder/jquery.placeholder.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/validator/jquery.form-validator.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/wow/wow.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/main.js');


		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition('t.fldate', strtotime(date('Y-m-d 00:00:00')), strtotime(date('Y-m-d 23:59:59')) + (3600 * 24 * 3));
		$criteria->order = 't.fldate ASC';
		Yii::import('manager.models.Schedule');
		$schedule = Schedule::model()->findAll($criteria);


		$arSchedule = array();
		foreach ($schedule as $item)
		{
			$arSchedule[Yii::app()->dateFormatter->format("LLLL, d", $item->fldate)][] = $item;
		}

		$this->render('index', array('schedule' => $arSchedule));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error)
		{
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionAuth()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->createUrl('/'.Yii::app()->user->role));
		$this->layout = 'auth';


		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'loginForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		$result = array('state' => 'ok', 'answer' => '');

		// collect user input data
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
			{
				if (Yii::app()->request->isAjaxRequest)
				{
					if (Yii::app()->user->role == 'employee')
						$result['answer'] = "/employee";
					else if (Yii::app()->user->role == 'contractor')
						$result['answer'] = "/contractor";
					else if (Yii::app()->user->role == 'manager')
						$result['answer'] = "/manager";
					else if (Yii::app()->user->role == 'administrator')
						$result['answer'] = "/administrator";

					echo CJSON::encode($result);
					Yii::app()->end();
				}
				else
					$this->redirect(Yii::app()->user->returnUrl);
			}
			else
			{
				$result['state']  = "error";
				$result['answer'] = "Невозможно выполнить вход";

				echo CJSON::encode($result);
				Yii::app()->end();
			}
		}

		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/modernizr-latest.js');
//		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/libs/jquery-1.11.0.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/bootstrap-3.1.1/js/bootstrap.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/nav/jquery.nav.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/placeholder/jquery.placeholder.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/plugins/validator/jquery.form-validator.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/static/js/auth.js');
		Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/static/css/auth.css');

		// display the login form
		$this->render('auth', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}


	public function actionOrder()
	{
		$model = new Order();

		$this->performAjaxValidation($model);

		$model->status_id  = OrderStatus::REQUESTED;
		$model->order_date = date('Y-m-d H:i:s');

		$model->contractor_id = null;

		if (!empty($_POST['Order']))
		{
			$model->attributes = $_POST['Order'];
			if ($model->save() && Yii::app()->request->isAjaxRequest)
			{
				$json['ok'] = 1;
				echo CJSON::encode($json);
				Yii::app()->end();
			}
		}
	}

	public function scheduleList()
	{
		Yii::import('manager.models.*');
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.fldate >= UNIX_TIMESTAMP()');
		$criteria->limit = 20;
		$criteria->order = 't.fldate ASC';

		$data     = Schedule::model()->with('flight')->findAll($criteria);
		$arResult = array(
			Yii::t('main', 'Flight number')
		);

		foreach ($data as $item)
		{
			$arResult[$item->id] = $item->getDatetime('d/m/Y H:i') . ' ' . $item->flight->flight;
		}

		return $arResult;
	}
}
